<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentProfile extends Model
{
    protected $table = 'student_profiles';

    // protected $primaryKey = "id_number";

    protected $fillable = [
        'id_number',
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'college',
        'course',
        'year_level',
        'contact_number',
        'guardian_contact_number',
        'umak_email',
        'profile_picture',
        'status'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function logs()
    {
        return $this->hasMany(StudentLog::class, 'id_number');
    }

    public function violations()
    {
        return $this->hasMany(StudentViolation::class, 'id_number');
    }

    public function getFirstNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getLastNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getCollegeAttribute($value)
    {
        return ucfirst($value);
    }

    public function getUmakEmailAttribute($value)
    {
        return ucfirst($value);
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
}
