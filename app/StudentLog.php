<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;


class StudentLog extends Model
{
    protected $table = 'student_logs';

    protected $fillable = ['id', 'id_number', 'time_in', 'time_out', 'created_at', 'updated_at'];

    protected $guard = [''];

    protected $hidden = ['created_at', 'updated_at'];


    public function student() {
        return $this->belongsTo(StudentProfile::class, 'id_number');
    }


    // public function getTimeInAttribute($value){
    //     return Carbon::parse($value)->format('d-M-Y H:i');
    // }

    // public function getTimeOutAttribute($value){
    //     return Carbon::parse($value)->format('d-M-Y H:i');
    // }
}
