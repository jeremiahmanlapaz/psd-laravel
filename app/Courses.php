<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $table = 'courses';

    protected $fillable = ['name', 'college', 'degree'];

    protected $hidden = ['created_at', 'updated_at', 'id'];
}
