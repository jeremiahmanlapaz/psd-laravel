<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colleges extends Model
{
    protected $table = 'colleges';

    protected $fillable = ['name', 'code'];

    protected $hidden = ['created_at', 'updated_at', 'id'];
}
