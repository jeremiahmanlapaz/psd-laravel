<?php

namespace App\Http\Controllers;

use App\Violations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function index()
    {
        if (url()->current() == url('admin/reports/violations')) {
            $violations = Violations::select('id', 'title')->get();
            return view('reports.violations_index', ['violations' => $violations]);
        } else {
            return view('reports.logs_index');
        }
    }

    public function getLogsReport(Request $request)
    {
        $in_start_date = $request->get('in_start_date');
        $in_end_date = $request->get('in_end_date');
        $out_start_date = $request->get('out_start_date');
        $out_end_date = $request->get('out_end_date');

        $data = DB::table('student_logs')
            ->select('*')
            ->whereBetween('time_in', [$in_start_date, $in_end_date])
            ->orderBy('time_in', 'desc')
            ->get();

        return $data;
    }

    public function getViolationReport(Request $request)
    {
        $year = $request->get('year') ?? '';
        $month = $request->get('month') ?? '';
        $college = $request->get('college');
        $type = $request->get('type');
        $year_level = $request->get('year_level');

        $data = DB::table('student_profiles')
            ->select(
                'student_profiles.id_number',
                'student_profiles.first_name',
                'student_profiles.last_name',
                'student_profiles.college',
                'student_profiles.course',
                'student_profiles.year_level',
                'student_profiles.gender',
                'violations.title',
                'student_violations.created_at',
                'student_violations.updated_at',
            )
            ->join('student_violations', 'student_profiles.id_number', '=', 'student_violations.id_number')
            ->join('violations', 'student_violations.violation_id', '=', 'violations.id')
            ->when($year, function ($query, $role) {
                return $query->whereRaw('YEAR(student_violations.created_at) = ?', $role);
            })
            ->when($college, function ($query, $role) {
                return $query->whereRaw('YEAR(student_violations.created_at) = ?', $role);
            })
            ->when($type, function ($query, $role) {
                return $query->where('violations.type', $role);
            })
            ->when($year_level, function ($query, $role) {
                return $query->whereRaw('YEAR(student_violations.created_at) = ?', $role);
            })
            ->when($month, function ($query, $role) {
                return $query->whereRaw('MONTH(student_violations.created_at) = ?', $role);
            })
            ->orderBy('student_violations.updated_at')
            // ->dd();
            ->get();

        return $data;
    }
}
