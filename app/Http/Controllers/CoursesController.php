<?php

namespace App\Http\Controllers;

use App\Courses;
use Illuminate\Http\Request;

class CoursesController extends Controller
{

    public function get($code)
    {
        $data = Courses::where('college', $code)->get();
        return $data;
    }

    public function import(Request $request)
    {
        $data = $request->all();
        foreach ($data as $college) {
            foreach ($college['course'] as $course) {
                Courses::create([
                    'college' => $college['college'],
                    'name' => $course['name'],
                    'degree' => $course['degree']
                ]);
            }
        }

        return $this->success();
    }
}
