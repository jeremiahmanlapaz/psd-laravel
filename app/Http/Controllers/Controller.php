<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function success($message = 'success', $data = [], $statusCode = 200)
    {
        if ($data == [] || $data == null) {
            return response()->json(['message' => $message], $statusCode);
        } else {
            return response()->json(['message' => $message, 'data' => $data], $statusCode);
        }
    }

    public function reject($message = 'reject', $data = [], $statusCode = 400)
    {
        if ($data == [] || $data == null) {
            return response()->json(['message' => $message], $statusCode);
        } else {
            return response()->json(['message' => $message, 'responseJSON' => $data], $statusCode);
        }
    }

    public function sendSMS($address, $message)
    {
        $short_code = config('services.glabs.short_code');
        $app_id = config('services.glabs.app_id');
        $app_secret = config('services.glabs.app_secret');
        $access_token = config('services.glabs.access_token');
        $base_uri = "https://devapi.globelabs.com.ph/";

        $client = new Client(['base_uri' => $base_uri]);
        $response = $client->request('POST', 'smsmessaging/v1/outbound/' . $short_code . '/requests', [
            'query' => ['access_token' => $access_token],
            'json' => [
                "outboundSMSMessageRequest" => [
                    "senderAddress" => $short_code,
                    'address' => $address,
                    'outboundSMSTextMessage' => ['message' => $message],
                ]
            ]
        ]);
        return $response->getStatusCode() == 201 ? $this->success() : $this->reject();
    }
}
