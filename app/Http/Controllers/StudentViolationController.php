<?php

namespace App\Http\Controllers;

use App\Violations;
use Illuminate\Http\Request;

class StudentViolationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('students.violations.violations_index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_number' => 'required',
            'violation_id' => 'required',
        ]);

        $student = StudentProfile::where('id_number', $request->id_number)->first();

        if (!$student) {
            return $this->reject('Student not found');
        }

        if ($student->status == "Expelled" || $student->status == "Suspended") {
            return $this->reject('Student is Expelled or Suspended');
        }

        $violation = Violations::find($request->violation_id);

        $data['id_number'] = $request->id_number;
        $data['violation_id'] = $request->violation_id;
        $data['expired_at'] = now()->addDays(5);

        if ($violation->type == "Major") {
            $data['sanction'] = $request->sanction;
        } elseif ($violation->type == "Minor") {
            $first = DB::table('student_violations')
                ->select(DB::raw('"Minor" as type'), DB::raw('COUNT(IFNULL(1, 0)) as value'))
                ->join('violations', 'violations.id', '=', 'student_violations.violation_id')
                ->where([
                    'student_violations.id_number' => $request->id_number,
                    'violations.type' => 'Minor',
                ])
                ->whereRaw('student_violations.violation_id = violations.id');

            $count = DB::table('student_violations')
                ->select(DB::raw('"Major" as type'), DB::raw('COUNT(IFNULL(1, 0)) as value'))
                ->join('violations', 'violations.id', '=', 'student_violations.violation_id')
                ->where([
                    'student_violations.id_number' => $request->id_number,
                    'student_violations.violation_id' => '$violations.id',
                    'violations.type' => 'Major',
                ])
                ->union($first)
                ->get();

            $sanction = DB::table('violations')
                ->select('first_offense', 'second_offense', 'third_offense')
                ->where('type', 'minor')
                ->groupBy('type')
                ->get()
                ->first();

            $minor_count = $count[1]->value + 1;

            if ($minor_count % 3 == 1) {
                $data['sanction'] = $sanction->first_offense;
            } else if ($minor_count % 3 == 2) {
                $data['sanction'] = $sanction->second_offense;
            } else if ($minor_count % 3 == 0) {
                $data['sanction'] = $sanction->third_offense;
                $data2['id_number'] = $request->id_number;
                $violation = DB::table('violations')->where('title', 'Repeated minor violation.')->first();
                $data2['violation_id'] = $violation->id;
                $data2['sanction'] = $violation->first_offense;
                $data2['expired_at'] = '';
            }
        }

        try {
            //SENDING EMAIL
            Mail::to($student->umak_email)->later(now()->addSeconds(30),new StudentViolationCreated($student));
        } catch (Exception $e) {
            return $this->reject('No Internet Connection');
        }

        $create = StudentViolation::create($data);

        if (isset($data2)) {
            StudentViolation::create($data2);
        }
        if ($create) {
            return $this->success('Violation Added');
        } else {
            return $this->reject('Violation Creation Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_number)
    {
        $violations = Violations::all();
        return view('students.violations.violations_show', ['id_number' => $id_number, 'violations' => $violations]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
