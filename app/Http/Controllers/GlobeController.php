<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class GlobeController extends Controller
{
    public $short_code, $app_id, $app_secret, $access_token, $base_uri;

    public function __construct()
    {
        $this->short_code = config('services.glabs.short_code');
        $this->app_id = config('services.glabs.app_id');
        $this->app_secret = config('services.glabs.app_secret');
        $this->access_token = config('services.glabs.access_token');
        $this->base_uri = "https://devapi.globelabs.com.ph/";
    }

    // public function sendSMS(Request $request)
    // {
    //     $client = new Client(['base_uri' => $this->base_uri]);
    //     // $sender = $this->short_code;
    //     $response = $client->request('POST', 'smsmessaging/v1/outbound/' . $this->short_code . '/requests', [
    //         'query' => ['access_token' => $this->access_token],
    //         'json' => [
    //             "outboundSMSMessageRequest" => [
    //                 "senderAddress" => $this->short_code,
    //                 'address' => $request->address,
    //                 'outboundSMSTextMessage' => [
    //                     'message' => $request->message
    //                 ],
    //             ]
    //         ]
    //     ]);
    //     return $response->getStatusCode() == 201 ? $this->success() : $this->reject();
    // }
}
