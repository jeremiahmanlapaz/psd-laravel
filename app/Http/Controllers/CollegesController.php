<?php

namespace App\Http\Controllers;

use App\Colleges;
use App\Courses;
use Illuminate\Http\Request;

class CollegesController extends Controller
{
    public function index()
    {
        return view('colleges.colleges_index');
    }

    public function importCollege(Request $request)
    {
        $data = json_decode(file_get_contents($request->file), true);
        foreach ($data as $college) {
            Colleges::create(['name' => $college['college'], 'code' => $college['code']]);
        }
        return back()->with(['message' => 'success', 'class' => 'success']);
    }

    public function all()
    {
        return Colleges::orderBy('name')->get();
    }

    public function importCourse(Request $request)
    {
        $data = json_decode(file_get_contents($request->file), true);
        foreach ($data as $college) {
            foreach ($college['course'] as $course) {
                Courses::create([
                    'college' => $college['college'],
                    'name' => $course['name'],
                    'degree' => $course['degree']
                ]);
            }
        }
        return back()->with(['message' => 'Import Success', 'class' => 'success']);
    }

    public function course($code)
    {
        return view('colleges.course_index', ['code' => $code]);
    }

    public function getCourse($code)
    {
        $data = Courses::where('college', $code)->get();
        return $data;
    }
}
