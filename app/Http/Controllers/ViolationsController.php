<?php

namespace App\Http\Controllers;

use App\Violations;
use Illuminate\Http\Request;

class ViolationsController extends Controller
{
    public function index()
    {
        return view('violations.violations_index');
    }

    public function create(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'type' => 'required',
            'first_offense' => 'required'
        ]);

        if (Violations::create($request->all())) {
            return $this->success('Violation Added');
        } else {
            return $this->reject('Violation Failed');
        }
    }

    public function showEdit($id)
    {
        $violation = Violations::find($id);
        return view('violations.violations_edit', ['violation' => $violation]);
    }

    public function edit($id, Request $request)
    {
        $request->validate([
            'title' => 'required',
            'type' => 'required',
            'first_offense' => 'required'
        ]);

        $violation = Violations::find($id)->update($request->except('_token'));

        if ($violation) {
            return redirect()->route('violations')->with(['message' => 'Violation Updated', 'class' => 'success']);
        } else {
            return redirect()->route('violations')->with(['message' => 'Violation Updated Failed', 'class' => 'success']);
        }
    }

    public function all($order = 'type')
    {
        return  Violations::orderBy($order)->get();
    }

    public function get($id)
    {
        $violation = Violations::find($id);
        $data = [
            $violation['first_offense'],
            $violation['second_offense'],
            $violation['third_offense'],
        ];
        return $data;
    }

    public function import(Request $request)
    {
        if ($request->hasFile('file')) {
            $data = json_decode(file_get_contents($request->file), true);
            foreach ($data as $violation) {
                Violations::create($violation);
            }
            return redirect()->route('violations')->with(['message' => 'Import Success', 'class' => 'success']);
        }
        return redirect()->route('violations')->with(['message' => 'File Not Found', 'class' => 'error']);
    }
}
