<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Phpml\Clustering\KMeans;

class ApiKmeansController extends Controller
{
    public function index(Request $request)
    {
    }

    public function compute(Request $request)
    {
        // if(!$request->has(['data', 'size'])){
        //     return $this->reject();
        // }

        //note $size must be greater than 0.
        $year = $request->get('year') ?? now()->year;
        $size = $request->get('s') ?? 4;

        $data = DB::table('student_violations')
            ->select('student_profiles.year_level', 'violations.id as violation_id', 'student_violations.id as student_violation_id')
            ->join('student_profiles', 'student_profiles.id_number', '=', 'student_violations.id_number')
            ->join('violations', 'violations.id', '=', 'student_violations.violation_id')
            ->when($request->get('college'), function ($query, $college) {
                return $query->where('student_profile.college', $college);
            })
            ->when($request->get('year_level'), function ($query, $year_level) {
                return $query->where('student_profile.year_level', $year_level);
            })
            ->when($year, function ($query, $year) {
                return $query->whereRaw('YEAR(student_violations.created_at) BETWEEN ? AND ?', [intval($year), $year + 1]);
            })
            ->orderBy('student_violations.id')
            // ->dd()
            ->get();

        if ($data === null || sizeof($data) == 0)
            return response()->json(["chart" => [], "table" => []]);


        $dataSet = array();
        foreach ($data as $item) {
            $elements = array();
            array_push($elements, (int)$item->year_level, (int)$item->violation_id);
            $dataSet[$item->student_violation_id] = $elements;
        }

        $kmeans = new KMeans($size, KMeans::INIT_RANDOM);
        $clusteredData = $kmeans->cluster($dataSet);

        // dd($clusteredData);

        // Return data for chart.js Ajax 

        $chartData = $this->chartData($clusteredData);

        $tableData = $this->tableData($clusteredData);

        return response()->json(["chart" => $chartData, "table" => $tableData]);
    }

    public function tableData($clusteredData)
    {
        $Ids = collect($clusteredData)->map(function ($item, $key) {
            return collect($item)->map(function ($item, $key) {
                return $key;
            });
        });

        $returnData = array();
        foreach ($Ids as $clusterKey => $value) {
            $clusterIds = array();

            foreach ($value as $key) {
                array_push($clusterIds, $key);
            }

            $returnData[$clusterKey] = DB::table('student_violations')
                ->select([
                    'student_profiles.id_number',
                    'student_profiles.first_name',
                    'student_profiles.last_name',
                    'student_profiles.college',
                    'student_profiles.course',
                    'student_profiles.year_level',
                    'student_violations.violation_id',
                    'student_violations.sanction'
                ])
                ->join('student_profiles', 'student_profiles.id_number', '=', 'student_violations.id_number')
                ->whereIn('student_violations.id', $clusterIds)
                ->get();
        }

        $return = array();
        foreach ($returnData as $key => $items) {
            foreach ($items as $value) {
                $value->cluster = $key;
                array_push($return, $value);
            }
        }
        return $return;
    }

    public function chartData($clusteredData)
    {
        $rgbColor = ['rgb(255,0,0)', 'rgb(255,255,0)', 'rgb(0,0,255)', 'rgb(0,128,0)'];
        $returnData = array();

        foreach ($clusteredData as $key => $cluster) {
            $returnData[$key]['label'] = 'Cluster ' . ($key + 1);
            $returnData[$key]['backgroundColor'] = $rgbColor[$key];

            $returnData[$key]['data'] = array();
            foreach ($cluster as $data) {
                array_push($returnData[$key]['data'], array('x' => $data[0], 'y' => $data[1]));
            }
        }

        return $returnData;
    }
}
