<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\StudentLog;
use App\StudentProfile;
use App\StudentViolation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiStudentLogController extends Controller
{
    public function create(Request $request)
    {
        if(!$request->has('id_number')) {
            return $this->reject('ID Number Required');
        }

        $id_number =  $request->get('id_number');

        $student = StudentProfile::where('id_number', $id_number)->first();

        if (!$student) {
            return $this->reject('Student Not Found');
        }

        $violations = $this->getViolation($student->id_number);
        $log = StudentLog::where('id_number', $id_number)->get()->last();
        // $address = $student->contact_number;
        // $address2 = $student->guardian_contact_number;

        foreach ($violations as $violation) {
            if (now()->greaterThanOrEqualTo($violation->expired_at)) {
                $message = 'Student has exceeds violation expiry date. Please bring parent and proceed to PSD.';
                // $this->sendSMS($address, $message);
                // $this->sendSMS($address2, $message);
                return $this->reject($message);
            }
        }
        if (!empty($log) && $log->time_out == null) {
            StudentLog::where('id', $log->id)->update(['time_out' => now()]);
            $message = 'SYSTEM ALERT: ' . $student->id_number . ': Time Out At ' . now()->format('d-M-Y H:i');
            $return_message = "Time-out successful";
        } else {
            StudentLog::create([
                'id_number' => $id_number,
                'time_in' => now()
            ]);
            $message = 'SYSTEM ALERT: ' . $student->id_number . ': Time In At ' . now()->format('d-M-Y H:i');
            $return_message = "Time-in successful";
        }
        // $this->sendSMS($address, $message);
        // $this->sendSMS($address2, $message);

        if (collect($violations)->isEmpty()) {
            return $this->success($return_message);
        } else {
            return $this->reject('Student has a pending violation(s), Please proceed to PSD.', ['hasViolation' => true]);
        }
    }

    public function get($id_number)
    {
        $data = StudentLog::where('id_number', $id_number)
            ->orderBy('updated_at', 'desc')
            ->get();

        return $data;
    }

    public function all(Request $request)
    {
        // dd(Carbon::create($request->get('year'))->format('Y-m-d h:m:s'));
        $data = DB::table('student_logs')
            ->select('student_profiles.first_name', 'student_profiles.last_name', 'student_logs.id_number', 'student_logs.time_in', 'student_logs.time_out')
            ->join('student_profiles', 'student_logs.id_number', '=', 'student_profiles.id_number')
            ->when($request->get('limit'), function ($query, $limit) {
                return $query->limit($limit);
            })
            ->when($request->get('year'), function ($query, $year) {
                return $query->whereRaw('DATE(student_logs.time_in) <= DATE(?)', Carbon::create($year)->format('Y-m-d h:m:s'));
            })
            ->orderBy('student_logs.time_in', 'desc')
            ->get();

        return $data;
    }

    public function getViolation($student_id)
    {
        $violations = StudentViolation::where('id_number', $student_id)
            ->whereNull('cleared_at')
            ->get();
        return $violations;
    }
}
