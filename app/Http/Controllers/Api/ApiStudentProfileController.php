<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\StudentProfile;
use Illuminate\Support\Facades\DB;

class ApiStudentProfileController extends Controller
{
    public function all()
    {
        return StudentProfile::all();
    }

    public function hasViolations()
    {
        $students = DB::table('student_profiles')
            ->select('student_profiles.*')
            ->groupBy('student_profiles.id_number')
            ->join('student_violations', 'student_profiles.id_number', '=', 'student_violations.id_number')
            ->whereNull('student_violations.cleared_at')
            ->get();

        return $students;
    }
}
