<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiAnalyticsController extends Controller
{

    public function getTotal(Request $request)
    {
        $year = $request->get('year') ? $request->get('year') : '2020';
        $month = $request->get('month');
        $college = $request->get('college');
        $violation = $request->get('violation');
        $year_level = $request->get('year_level');

        $chart = DB::table('student_violations')
            ->select(DB::raw('DATE_FORMAT(student_violations.created_at, "%M") as category'), DB::raw('count(student_violations.id_number) as value'))
            ->join('student_profiles', 'student_profiles.id_number', '=', 'student_violations.id_number')
            ->join('violations', 'violations.id', '=', 'student_violations.violation_id')
            ->when($year, function ($query, $role) {
                return $query->whereRaw('YEAR(student_violations.created_at) = ?', $role);
            })
            ->when($month, function ($query, $role) {
                return $query->whereRaw('MONTH(student_violations.created_at) = ?', $role);
            })
            ->when($college, function ($query, $role) {
                return $query->where('student_profiles.college', $role);
            })
            ->when($year_level, function ($query, $role) {
                return $query->where('student_profiles.year_level', $role);
            })
            ->when($violation, function ($query, $role) {
                return $query->where('violations.id', $role);
            })
            ->groupBy(DB::raw('MONTH(student_violations.created_at)'))
            // ->dd();
            ->get();
        $test = [];

        for ($i = 0; $i < 12; $i++) {
            $test[$i]['category'] = Carbon::create($year, $i + 1)->format('M');
            foreach ($chart as $data) {
                if (Carbon::create($data->category)->format('M') == $test[$i]['category']) {
                    $test[$i]['value'] = $data->value;
                    break;
                } else {
                    $test[$i]['value'] = 0;
                }
            }
        }
        // dd($test, $chart);
        return response()->json($test);
    }

    public function getTotalbyMonth(Request $request)
    {
        $year = $request->get('year') ? $request->get('year') : '2020';
        $month = $request->get('month');
        $college = $request->get('college');
        $year_level = $request->get('year_level');

        $data = DB::table('student_violations')
            ->select(DB::raw('COUNT(student_violations.id_number) as value'))
            ->join('student_profiles', 'student_violations.id_number', '=', 'student_profiles.id_number')
            ->when($year, function ($query, $role) {
                return $query->whereRaw('YEAR(student_violations.created_at) = ?', $role);
            })
            ->when($month, function ($query, $role) {
                return $query->whereRaw('MONTH(student_violations.created_at) = ?', $role);
            })
            ->when($college, function ($query, $role) {
                return $query->where('student_profiles.college', $role);
            })
            ->when($year_level, function ($query, $role) {
                return $query->where('student_profiles.year_level', $role);
            })
            ->get();

        return $data;
    }

    public function getTotalbyTitle(Request $request)
    {
        $year = $request->get('year') ? $request->get('year') : '2020';
        $month = $request->get('month');
        $college = $request->get('college');
        $year_level = $request->get('year_level');

        $data = DB::table('student_violations')
            ->select(DB::raw('COUNT(student_violations.violation_id) as value'), DB::raw('SUBSTRING(violations.title, 1, 50) as category'))
            ->join('violations', 'violations.id', '=', 'student_violations.violation_id')
            ->when($year, function ($query, $role) {
                return $query->whereRaw('YEAR(student_violations.created_at) = ?', $role);
            })
            ->when($month, function ($query, $role) {
                return $query->whereRaw('MONTH(student_violations.created_at) = ?', $role);
            })
            ->when($college, function ($query, $role) {
                return $query->where('student_profiles.college', $role);
            })
            ->when($year_level, function ($query, $role) {
                return $query->where('student_profiles.year_level', $role);
            })
            ->groupBy('violations.title')
            ->get();

        return $data;
    }

    public function getTotalbyType(Request $request)
    {
        $year = $request->get('year') ? $request->get('year') : '2020';
        $month = $request->get('month');
        $college = $request->get('college');
        $year_level = $request->get('year_level');

        $data = DB::table('student_violations')
            ->select(DB::raw('COUNT(student_violations.id) as value'), 'violations.type as category')
            ->join('violations', 'student_violations.violation_id', '=', 'violations.id')
            ->join('student_profiles', 'student_violations.id_number', '=', 'student_profiles.id_number')
            ->when($year, function ($query, $role) {
                return $query->whereRaw('YEAR(student_violations.created_at) = ?', $role);
            })
            ->when($month, function ($query, $role) {
                return $query->whereRaw('MONTH(student_violations.created_at) = ?', $role);
            })
            ->when($college, function ($query, $role) {
                return $query->where('student_profiles.college', $role);
            })
            ->when($year_level, function ($query, $role) {
                return $query->where('student_profiles.year_level', $role);
            })
            ->groupBy('violations.type')
            // ->dd();
            ->get();

        return json_encode($data);
    }

    public function getTotalbyGender(Request $request)
    {
        $year = $request->get('year') ? $request->get('year') : '2020';
        $month = $request->get('month');
        $college = $request->get('college');
        $year_level = $request->get('year_level');

        $data = DB::table('student_violations')
            ->select(DB::raw('COUNT(student_profiles.gender) as value'), 'student_profiles.gender as category')
            ->join('student_profiles', 'student_profiles.id_number', '=', 'student_violations.id_number')
            ->when($year, function ($query, $role) {
                return $query->whereRaw('YEAR(student_violations.created_at) = ?', $role);
            })
            ->when($month, function ($query, $role) {
                return $query->whereRaw('MONTH(student_violations.created_at) = ?', $role);
            })
            ->when($college, function ($query, $role) {
                return $query->where('student_profiles.college', $role);
            })
            ->when($year_level, function ($query, $role) {
                return $query->where('student_profiles.year_level', $role);
            })
            ->groupBy('student_profiles.gender')
            ->get();
        // ->dd();

        return json_encode($data);
    }
}
