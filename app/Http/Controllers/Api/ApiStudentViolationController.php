<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\StudentProfile;
use App\StudentViolation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiStudentViolationController extends Controller
{
    public function all(Request $request)
    {
        $data = DB::table('student_violations')
            ->select('student_violations.id_number', 'student_violations.sanction', 'student_violations.expired_at', 'student_violations.cleared_at', 'student_violations.status', 'violations.type', 'violations.title')
            ->join('violations', 'violations.id', '=', 'student_violations.violation_id')
            ->when($request->has('limit'), function ($query, $limit) {
                return $query->limit($limit);
            })->get();

        return $data;
    }

    public function get($id_number)
    {
        $data = DB::table('student_violations')
            ->select('student_violations.*', 'violations.type', 'violations.title')
            ->join('violations', 'violations.id', '=', 'student_violations.violation_id')
            ->where('student_violations.id_number', $id_number)
            ->get();
        return $data;
    }

    public function clear(Request $request)
    {
        if(!$request->has('id')){
            return $this->reject("Violation not Found");
        }

        $id = $request->get('id');

        $message = "Violation Cleared";
        $violation = StudentViolation::find($id);
        if ($violation) {
            $student = StudentProfile::where('id_number', $violation->id_number)->first();
            $violation->cleared_at = now();
            $violation->status = 'cleared';
            $violation->save();
            // $this->sendSMS($student->contact_number, $message);

            return $this->success($message);
        } else {
            $message = "Violation not Found";
            return $this->success($message);
        }
    }
}
