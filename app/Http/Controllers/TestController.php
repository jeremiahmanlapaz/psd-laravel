<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function index()
    {
        return view('test');
    }

    public function truncate()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('student_profiles')->truncate();
            DB::table('student_violations')->truncate();
            DB::table('student_logs')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        });

        return $this->success();
    }

    public function showToken()
    {
        return csrf_token();
    }
}
