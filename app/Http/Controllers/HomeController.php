<?php

namespace App\Http\Controllers;

use App\Colleges;
use App\StudentProfile;
use App\StudentViolation;
use App\Violations;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_students = StudentProfile::all()->count();
        $violation_month = StudentViolation::whereRaw('YEAR(student_violations.created_at) = ?', now()->year)
            ->whereRaw('MONTH(student_violations.created_at) = ?', now()->month)
            ->count();
        $college = Colleges::all();
        $pending = StudentViolation::where('status', 'pending')->count();
        $cleared = StudentViolation::where('status', 'cleared')->count();
        return view('home', [
            'total_students' => $total_students,
            'violation_month' => $violation_month,
            'colleges' => $college,
            'pending' => $pending,
            'clear' => $cleared
        ]);
    }
}
