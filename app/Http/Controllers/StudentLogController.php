<?php

namespace App\Http\Controllers;

use App\StudentLog;
use App\StudentProfile;
use Illuminate\Http\Request;

class StudentLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('students.logs.logs_index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StudentLog  $studentLog
     * @return \Illuminate\Http\Response
     */
    public function show($studentId)
    {
        return view('students.logs.logs_show', ['id_number' => $studentId]);        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudentLog  $studentLog
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentLog $studentLog)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StudentLog  $studentLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentLog $studentLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudentLog  $studentLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentLog $studentLog)
    {
        //
    }
}
