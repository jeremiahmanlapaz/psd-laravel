<?php

namespace App\Http\Controllers;

use App\Mail\SendAnnouncement;
use App\StudentProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AnnouncementController extends Controller
{
    public function index()
    {
        return view('annoucement.index');
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max: 255',
            'body' => 'required|max: 255',
        ]);
        $students = StudentProfile::take(10)->get();
        // dd($students);
        foreach ($students as $student) {
            $mail = Mail::to($student->umak_email)->later(now()->addSeconds(30), new SendAnnouncement($student, $request->title, $request->body));
        }
        return back()->with(['class' => 'success', 'message' => 'Successful Send']);
    }
}
