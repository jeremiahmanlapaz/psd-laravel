<?php

namespace App\Http\Controllers;

use App\StudentProfile;
use Illuminate\Http\Request;
use App\Imports\StudentsImport;
use Maatwebsite\Excel\Facades\Excel;


class StudentProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('students.profiles.profiles_index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.profiles.profiles_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_number' => 'required|unique:student_profiles,id_number',
            'profile_picture' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'college' => 'required',
            'course' => 'required',
            'contact_number' => 'required',
            'guardian_contact_number' => 'required',
            'year_level' => 'required',
            'umak_email' => 'required',
        ]);
        $path = $request->file('profile_picture')->store('');

        $data = $request->except('_token');
        $data['profile_picture'] = $path;

        if (StudentProfile::create($data)) {
            return redirect()->route('profiles.index')->with(['message' => 'Success']);
        }else{
            return redirect()->back()->with(['message' => 'Something went wrong']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_number)
    {
        $profile = StudentProfile::where('id_number', $id_number)->first();
        return view('students.profiles.profile_show', ['profile' => $profile]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function import(Request $request)
    {
        $import = Excel::import(new StudentsImport, $request->file('file'));
        if ($import) {
            return back()->with(['message' => 'Import Successful', 'class' => 'success']);
        } else {
            return back()->with(['message' => 'Import Failed', 'class' => 'error']);
        }
    }
}
