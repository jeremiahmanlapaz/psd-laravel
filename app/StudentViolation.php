<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentViolation extends Model
{
    protected $table = 'student_violations';

    protected $fillable = [
        'id_number',
        'violation_id',
        'sanction',
        'expired_at',
        'status',
        'cleared_at'
    ];

    protected $guard = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function student()
    {
        return $this->belongsTo(StudentProfile::class);
    }
}
