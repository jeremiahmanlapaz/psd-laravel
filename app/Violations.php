<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Violations extends Model
{
    protected $table = 'violations';

    protected $fillable = [
        'id',
        'title',
        'type',
        'first_offense',
        'second_offense',
        'third_offense',
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function getFirstOffenseAttribute($value)
    {
        return $value;
    }
}
