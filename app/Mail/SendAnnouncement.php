<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendAnnouncement extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $student;
    protected $title;
    protected $body;

    public function __construct($student, $title, $body)
    {
        $this->student = $student;
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = $this->title;
        $body = $this->body;

        return $this->view('mails.announcement', ['student' => $this->student, 'title' => $title, 'body' => $body]);
    }
}
