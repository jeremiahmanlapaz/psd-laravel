<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home.welcome');
});

Route::get('/admin', function () {
    return redirect()->route('admin.home');
});

Route::get('/timecheck', function () {
    return now()->format('d m y, h:m:s');
});

Route::group(['prefix' => 'admin',], function () {

    Auth::routes();

    Route::middleware(['auth'])->group(function () {

        Route::get('/dashboard', 'HomeController@index')->name('admin.home');

        Route::group(['prefix' => 'students'],  function () {

            Route::resource('logs', StudentLogController::class);

            Route::post('/profiles/import', 'StudentProfileController@import')->name('profiles.import');

            Route::resource('profiles', StudentProfileController::class);

            Route::resource('violations', StudentViolationController::class);
        });

        Route::get('/analytics/cluster', 'AnalyticsController@cluster')->name('analytics.cluster');

        Route::resource('/analytics', AnalyticsController::class);

        Route::group(['prefix' => 'violations'], function () {

            Route::get('/', 'ViolationsController@index')->name('violations');

            Route::get('/all/{order?}', 'ViolationsController@all')->name('violations.all');

            Route::post('/create', 'ViolationsController@create')->name('violations.create');

            Route::get('/edit/{id}', 'ViolationsController@showEdit')->name('violations.edit');

            Route::post('/edit/{id}', 'ViolationsController@edit');

            Route::get('/get/{id}', 'ViolationsController@get')->name('violations.get');

            Route::post('/import', 'ViolationsController@import')->name('violations.import');
        });

        Route::group(['prefix' => 'reports'], function () {

            Route::get('/violations', 'ReportController@index')->name('reports.violations');

            Route::get('/logs', 'ReportController@index')->name('reports.logs');

            Route::get('/logs/get', 'ReportController@getLogsReport')->name('reports.logs.get');

            Route::get('/violations/get', 'ReportController@getViolationReport')->name('reports.violations.get');
        });

        Route::group(['prefix' => 'colleges'], function () {

            Route::get('/', 'CollegesController@index')->name('colleges');

            Route::get('/course/{code}', 'CollegesController@course')->name('colleges.course');

            Route::post('/import/college', 'CollegesController@importCollege')->name('colleges.import.college');

            Route::post('/import/course', 'CollegesController@importCourse')->name('colleges.import.course');

            Route::get('/all', 'CollegesController@all')->name('colleges.all');

            Route::get('/get/course/{code}', 'CollegesController@getCourse')->name('colleges.get.course');
        });

        Route::group(['prefix' => 'globe'], function () {

            Route::post('/send', 'GlobeController@sendSMS')->name('globe.send');
        });

        Route::group(['prefix' => 'test'], function () {

            Route::get('/', 'TestController@index')->name('test');

            Route::get('/csrf', 'TestController@showToken')->name('test.csrf');

            Route::get('/truncate', 'TestController@truncate')->name('test.truncate');
        });

        Route::group(['prefix' => 'announcement'], function () {

            Route::get('/', 'AnnouncementController@index')->name('announcement');

            Route::post('/send', 'AnnouncementController@send')->name('announcement.send');
        });
    });
});
