<?php

use App\Http\Controllers\Api\ApiAnalyticsController;
use App\Http\Controllers\Api\ApiKmeansController;
use App\Http\Controllers\Api\ApiStudentLogController;
use App\Http\Controllers\Api\ApiStudentProfileController;
use App\Http\Controllers\Api\ApiStudentViolationController;
use App\Http\Controllers\CoursesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'logs'], function () {

    Route::get('/all', [ApiStudentLogController::class, 'all']);

    Route::get('/get/{id_number}', [ApiStudentLogController::class, 'get']);

    Route::post('/log', [ApiStudentLogController::class, 'create']);
});

Route::group(['prefix' => 'profiles'], function () {

    Route::get('/all', [ApiStudentProfileController::class, 'all']);

    Route::get('/violations', [ApiStudentProfileController::class, 'hasViolations']);
});

Route::group(['prefix' => 'violations'], function () {

    Route::get('/all', [ApiStudentViolationController::class, 'all']);

    Route::get('/get/{id_number}', [ApiStudentViolationController::class, 'get']);

    Route::post('/clear', [ApiStudentViolationController::class, 'clear']);
});

Route::group(['prefix' => 'analytics'], function () {

    Route::get('/total', [ApiAnalyticsController::class, 'getTotal'])->name('analytics.total');

    Route::get('/total/month', [ApiAnalyticsController::class, 'getTotalbyMonth'])->name('analytics.total.month');

    Route::get('/total/gender', [ApiAnalyticsController::class, 'getTotalbyGender'])->name('analytics.total.gender');

    Route::get('/total/type', [ApiAnalyticsController::class, 'getTotalbyType'])->name('analytics.total.type');

    Route::get('/total/title', [ApiAnalyticsController::class, 'getTotalbyTitle'])->name('analytics.total.title');
});

Route::group(['prefix' => 'kmeans'], function () {

    Route::get('/compute', [ApiKmeansController::class, 'compute']);

    Route::get('/compute/table', [ApiKmeansController::class, 'table']);
});

Route::group(['prefix' => 'courses'], function () {

    Route::get('/get/{code}', [CoursesController::class, 'get'])->name('api.courses.get');

    Route::post('/import', [CoursesController::class, 'import']);
});
