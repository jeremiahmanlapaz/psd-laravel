<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Colleges;
use App\Courses;
use App\StudentProfile;
use Faker\Generator as Faker;

$factory->define(StudentProfile::class, function (Faker $faker) {
    $idNumber = 'K11'.$faker->randomNumber(6);
    $gender = collect(['male', 'female'])->random();
    $firstName = $faker->firstName($gender);
    $lastName = $faker->lastName();
    $college = Colleges::all()->random(1)->first();
    $course = Courses::where('college', $college->code)->get()->random(1)->first();
    return [
        'id_number' => $idNumber,
        'first_name' => $firstName,
        'middle_name' => $faker->lastName(),
        'last_name' => $lastName,
        'gender' => $gender,
        'college' => $college->code,
        'course' => $course->name,
        'year_level' => $faker->numberBetween(1, 4),
        // 'contact_number' => $faker->phoneNumber(),
        'contact_number' => '09276387225',
        'guardian_contact_number' => $faker->phoneNumber(),
        'umak_email' => $idNumber.'.'.substr($firstName,0,1).$lastName.'@umak.edu.ph',
    ];
});
