<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\StudentProfile;
use App\StudentViolation;
use App\Violations;
use Carbon\Carbon;
use Faker\Generator as Faker;

class StudentViolationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $limit  = 150;
        $students = StudentProfile::take($limit)->get();

        $year_until = now()->year;
        $year_before = now()->subYears(3)->year;

        for ($y = $year_before; $y <= $year_until; $y++) {
            $date = Carbon::create($y, rand(1, 12));
            foreach ($students as $key => $student) {
                if ($key < 75) {
                    $violation = Violations::where('type', 'minor')->get()->random();
                } else {
                    $violation = Violations::where('type', 'major')->get()->random();
                }
                $data['id_number'] = $student['id_number'];
                $data['violation_id'] = $violation['id'];
                $data['sanction'] = $violation['first_offense'];
                $data['created_at'] = $date;
                $data['updated_at'] = $date;

                // No office in weekends
                $expired_date = $date->addDays(5);

                if ($expired_date->isSaturday()) {
                    $data['expired_at'] = $expired_date->addDays(2);
                } elseif ($expired_date->isSunday()) {
                    $data['expired_at'] = $expired_date->addDays(1);
                } else {
                    $data['expired_at'] = $expired_date;
                }

                StudentViolation::create($data);
            }
        }
    }
}
