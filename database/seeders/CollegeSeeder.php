<?php

namespace Database\Seeders;


use App\Colleges;
use Illuminate\Database\Seeder;

class CollegeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = file_get_contents(storage_path() . '/app/json/Colleges.json');

        $data = json_decode($file, true);

        foreach ($data as $value) {
            Colleges::create([
                'name' => $value['college'],
                'code' => $value['code']
            ]);
        }
    }
}
