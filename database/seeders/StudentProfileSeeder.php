<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Colleges;
use App\Courses;
use App\StudentProfile;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

class StudentProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        // $code = $this->getCollege()->code;

        // echo $this->getCourse($code);

        $max = 100;

        // Loop through year level
        for ($yearlevel = 1; $yearlevel < 5; $yearlevel++) {
            for ($i = 0; $i < $max; $i++) {
                $college = Colleges::all()->random();
                $course = Courses::where('college', $college['code'])->first();
                // $rand_num = random_int(0, 99999);
                $profile['id_number']               = "K11" . str_pad($yearlevel . $i, 5, '0', STR_PAD_LEFT);
                $profile['first_name']              = $faker->firstName();
                $profile['middle_name']             = $faker->lastName();
                $profile['last_name']               = $faker->lastName();
                $profile['gender']                  = Arr::random(['Male', 'Female']);
                $profile['college']                 = $college['code'];
                $profile['course']                  = $course->name;
                $profile['year_level']              = $yearlevel;
                $profile['contact_number']          = '09260918342';
                $profile['guardian_contact_number'] = '09260918342';
                $profile['umak_email']              = substr($profile['first_name'], 0, 1) . '' . $profile['last_name'] . '.' . $profile['id_number'] . '@umak.edu.ph';
                StudentProfile::create($profile);
            }
        }
    }

    public function getCollege()
    {
        return Colleges::all()->random();
    }

    public function getCourse($code)
    {
        $course = Courses::where('college', $code)->get();
        return $course->random();
    }
}
