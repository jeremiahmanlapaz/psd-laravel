<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\StudentProfile::class, 50)->create();
        $this->call(UserSeeder::class);
        $this->call(CollegeSeeder::class);
        $this->call(CourseSeeder::class);
        $this->call(ViolationSeeder::class);
        $this->call(StudentProfileSeeder::class);
        $this->call(StudentLogSeeder::class);
        $this->call(StudentViolationSeeder::class);
    }
}
