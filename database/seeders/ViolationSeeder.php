<?php

namespace Database\Seeders;

use App\Violations;
use Illuminate\Database\Seeder;

class ViolationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = file_get_contents(storage_path() . '/app/json/Violations.json');

        $data = json_decode($file, true);

        foreach ($data as $value) {
            Violations::create($value);
        }
    }
}
