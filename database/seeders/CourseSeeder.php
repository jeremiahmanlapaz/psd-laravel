<?php

namespace Database\Seeders;

use App\Courses;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = file_get_contents( storage_path().'/app/json/Course.json');

        $data = json_decode($file, true);

        foreach ($data as $course) {
            foreach ($course['course'] as $key) {
                Courses::create([
                    'college' => $course['college'],
                    'name'    => $key['name'],
                    'degree'  => $key['degree']
                ]);
            }
        }
    }
}
