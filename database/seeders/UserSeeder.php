<?php

namespace Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin'),
            'role' => 'admin',
            'is_super' => true,
        ]);

        User::create([
            'name' => 'super_admin',
            'email' => 'superadmin@admin.com',
            'password' => Hash::make('superadmin'),
            'role' => 'admin',
            'is_super' => true,
        ]);
    }
}
