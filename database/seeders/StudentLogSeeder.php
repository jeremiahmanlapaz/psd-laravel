<?php

namespace Database\Seeders;

use App\StudentLog;
use App\StudentProfile;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StudentLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $start_year = 2015;

        $end_year = 2021;

        $student_count = 10;

        for ($year = $start_year; $year <= $end_year; $year++) {

            for ($month = 1; $month <= 12; $month++) {

                $daysInMonth = Carbon::create($year, $month)->daysInMonth;

                for ($j = 1; $j < $daysInMonth; $j++) {

                    $students = StudentProfile::all()->random($student_count);

                    foreach ($students as $student) {

                        $start_date = Carbon::create($year, $month, $j, 8, 0);

                        $end_date = Carbon::create($year, $month, $j, 17, 0);

                        if ($start_date->toDateString() == now()->toDateString()) {
                            break 4;
                        }

                        StudentLog::create([
                            'id_number' => $student->id_number,
                            'time_in' => $start_date,
                            'time_out' => $end_date
                        ]);
                    }
                }
            }
        }
    }
}
