<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentViolationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_violations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_number');
            $table->foreign('id_number')->references('id_number')->on('student_profiles');
            $table->unsignedBigInteger('violation_id');
            $table->foreign('violation_id')->references('id')->on('violations');
            $table->string('sanction')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->string('status')->default('pending');
            $table->timestamp('cleared_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_violations');
    }
}
