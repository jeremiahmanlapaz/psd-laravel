<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_profiles', function (Blueprint $table) {
            // $table->bigIncrements('id');
            $table->string('id_number')->primary();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('gender')->nullable();
            $table->string('college')->nullable();
            $table->string('course')->nullable();
            $table->string('year_level')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('guardian_contact_number')->nullable();
            $table->string('umak_email')->nullable();
            $table->string('profile_picture')->default('default.png');
            $table->string('status')->default('student');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_profiles');
    }
}
