@extends('layouts.admin')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Analytics / Cluster </h6>
        </div>
        <div class="card-body">
            <div class="row">
                {{-- YEAR --}}
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Academic Year</label>
                        <select class="form-control form-control-sm" id="year">
                            <option value="2020">2020-2021</option>
                            <option value="2019">2019-2020</option>
                            <option value="2018">2018-2019</option>
                            <option value="2017">2017-2018</option>
                        </select>
                    </div>
                </div>
                {{-- COLLEGE --}}
                <div class="col-md-3">
                    <div class="form-group">
                        <label>College</label>
                        <select class="form-control form-control-sm" id="college">
                            <option value="">ALL</option>
                            @foreach ($colleges as $college)
                                <option value="{{ $college->code }}">{{ $college->code }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                {{-- YEAR LEVEL --}}
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Year Level</label>
                        <select class="form-control form-control-sm" id="year_level">
                            <option value="">ALL</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <table class="table table-sm" id="table" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID Number</th>
                                <th>Cluster</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>College</th>
                                <th>Course</th>
                                <th>Sanction</th>
                                <th>Year Level</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-md-12" style="max-width: 50%;">
                    <canvas id="myChart" width="150" height="150"></canvas>
                </div>
            </div> --}}
        </div>
    </div>
@endsection

@section('js')
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.min.js"
        integrity="sha512-RGbSeD/jDcZBWNsI1VCvdjcDULuSfWTtIva2ek5FtteXeSjLfXac4kqkDRHVGf1TwsXCAqPTF7/EYITD0/CTqw=="
        crossorigin="anonymous"></script> --}}
    <script>
        $(document).ready(function() {

            let table = $('#table').DataTable({
                ajax: {
                    url: "/api/kmeans/compute",
                    dataSrc: 'table'
                },
                order: [
                    [1, 'asc']
                ],
                columns: [{
                        data: 'id_number'
                    },
                    {
                        data: 'cluster',
                        render: function(d) {
                            return d + 1;
                        }
                    },
                    {
                        data: 'first_name'
                    },
                    {
                        data: 'last_name'
                    },
                    {
                        data: 'college'
                    },
                    {
                        data: 'course'
                    },
                    {
                        data: 'sanction'
                    },
                    {
                        data: 'year_level',
                    },
                ],
                dom: 'Bfrtip',
                buttons: [{
                        text: '<i class="fas fa-file-csv"></i> Save as CSV',
                        extend: 'csv',
                        className: 'btn btn-success mx-1 rounded-pill',
                        title: 'Report List'
                    },
                    {
                        text: '<i class="fas fa-file-pdf"></i> Save as PDF',
                        extend: 'pdf',
                        className: 'btn btn-success mx-1 rounded-pill',
                        title: 'Report List'
                    },
                ]
            });

            table.on('draw.dt', function() {
                console.log("table data", table.data());
            });

            $('#year, #college, #year_level').change(function() {
                var college = $('#college').val();
                var year = $('#year').val();
                var year_level = $('#year_level').val();
                getData(year, college, year_level);
            });

            function getData(year = '', college = '', year_level = '') {
                var url = '/api/kmeans/compute?year=:year&college=:college&year_level=:year_level'
                url = url.replace(':year', year).replace(':college', college).replace(':year_level', year_level);
                $.ajax({
                    url: url,
                    success: function(d) {
                        console.log("ajax call", d);
                        table.fnClearTable();
                        table.fnAddData(d.table);
                        // chart.config.data.datasets = d.chart;
                        // chart.update();
                    }
                });
            }

            // const ctx = $('#myChart');
            // const config = {
            //     type: 'scatter',
            //     options: {
            //         scales: {
            //             x: {
            //                 type: 'linear',
            //                 position: 'bottom',
            //                 title: 'Year level'
            //             },
            //             y: {
            //                 type: 'linear',
            //                 position: 'start',
            //                 title: 'Violation'
            //             }
            //         }
            //     }
            // };
            // var chart = new Chart(ctx, config);

            // getData();
        });

    </script>

@endsection
