<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PRINT</title>
</head>
<body>

    <script src="{{ asset('vendors/amcharts4/core.js') }}"></script>
    <script src="{{ asset('vendors/amcharts4/charts.js') }}"></script>
    <script src="{{ asset('vendors/amcharts4/themes/animated.js') }}"></script>
    <script src="{{ asset('vendors/amcharts4/plugins/sliceGrouper.js') }}"></script>
</body>
</html>
