@extends('layouts.admin')

@section('content')
<!-- Basic Card Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Analytics / Charts</h6>
    </div>
    <div class="card-body overflow-auto">
        <div class="row">
            {{-- YEAR --}}
            <div class="col-md-3">
                <div class="form-group">
                    <label>Academic Year</label>
                    <select class="form-control form-control-sm" id="chart2_year">
                        <option value="2020">2020-2021</option>
                        <option value="2019">2019-2020</option>
                        <option value="2018">2018-2019</option>
                        <option value="2017">2017-2018</option>
                    </select>
                </div>
            </div>
            {{-- COLLEGE --}}
            <div class="col-md-3">
                <div class="form-group">
                    <label>College</label>
                    <select class="form-control form-control-sm" id="chart2_college">
                        <option value="">ALL</option>
                        @foreach ($colleges as $college)
                        <option value="{{$college->code}}">{{$college->code}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            {{-- YEAR LEVEL --}}
            <div class="col-md-3">
                <div class="form-group">
                    <label>Year Level</label>
                    <select class="form-control form-control-sm" id="chart2_year_level">
                        <option value="">ALL</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="lineChart" style="height: 500px;"></div>
            </div>
            <div class="col-md-6">
                <div id="genderChart" style="height: 500px;"></div>
            </div>
            <div class="col-md-6">
                <div id="typeChart" style="height: 400px;"></div>
            </div>
            <div class="col-md-12">
                <div id="titleChart" style="min-height: 600px;"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        initLineChartData();
        initColumnChartData();
        initPieChartData();
        initColumnChartData2();
        $('#chart2_year, #chart2_college, #chart2_violation, #chart2_year_level').change(function () {
            let y = $('#chart2_year').val();
            let c = $('#chart2_college').val();
            let v = $('#chart2_violation').val();
            let yl = $('#chart2_year_level').val();
            initLineChartData(y, c, v, yl);
            initLineChartData(y, c, v, yl);
            initColumnChartData(y, c, v, yl);
            initPieChartData(y, c, v, yl);
            initColumnChartData2(y, c, v, yl);
        });
    });

    function initLineChartData(y = 2020, c = "", v = "", yl = "") {
        $.ajax({
            url: "{{route('analytics.total')}}",
            dataType: 'JSON',
            data: {year: y, college: c, violation: v, year_level: yl},
            complete: (j, s) => {
                if(j.statusText == "OK") {
                    initLineChart(j.responseJSON);
                }
            }
        });
    }

    function initColumnChartData(y = 2020, c = "", v = "", yl = "") {
        $.ajax({
            url: "{{route('analytics.total.gender')}}",
            dataType: 'JSON',
            data: {year: y, college: c, violation: v, year_level: yl},
            complete: (j, s) => {
                if(j.statusText == "OK") {
                    initColumnChart(j.responseJSON, "genderChart", "Total Violations by Gender");
                }
            }
        });
    }

    function initColumnChartData2(y = 2020, c = "", v = "", yl = "") {
        $.ajax({
            url: "{{route('analytics.total.title')}}",
            dataType: 'JSON',
            data: {year: y, college: c, violation: v, year_level: yl},
            complete: (j, s) => {
                if(j.statusText == "OK") {
                    initColumnChart(j.responseJSON, "titleChart", "Total violations");
                }
            }
        });
    }

    function initPieChartData(y = 2020, c = "", v = "", yl = "") {
        $.ajax({
            url: "{{route('analytics.total.type')}}",
            dataType: 'JSON',
            data: {year: y, college: c, violation: v, year_level: yl},
            complete: (j, s) => {
                if(j.statusText == "OK") {
                    initPieChart(j.responseJSON);
                }
            }
        });
    }

    function initPieChart(data) {
        let chart = am4core.create("typeChart", am4charts.PieChart);
        chart.data = data;
        chart.exporting.menu = new am4core.ExportMenu();
        //Chart Title
        let title = chart.titles.create();
        title.text = "Total Violations by Type";
        // Create series
        var series = chart.series.push(new am4charts.PieSeries());
        series.name = "Violation by Type";
        series.dataFields.value = "value";
        series.dataFields.category = "category";
    }

    function initColumnChart(data, element, chartTitle) {
        let chart = am4core.create(element, am4charts.XYChart);
        chart.data = data;
        chart.exporting.menu = new am4core.ExportMenu();

        let title = chart.titles.create();
        title.text = chartTitle;

        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        // categoryAxis.title.text = Title;
        categoryAxis.renderer.labels.template.disabled = true;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Number";

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.name = "Violation by Gender";
        series.dataFields.valueY = "value";
        series.dataFields.categoryX = "category";
        let category = "{category}";
        series.columns.template.tooltipText =  `{categoryX}:{valueY}`;
    }

    function initLineChart (data) {
        let chart = am4core.create("lineChart", am4charts.XYChart);
        chart.data = data;
        chart.exporting.menu = new am4core.ExportMenu();

        let title = chart.titles.create();
        title.text = "Total Violations by Month";

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.title.text = "Month";
        categoryAxis.renderer.minGridDistance = 20;
        categoryAxis.renderer.line.strokeOpacity = 1;
        categoryAxis.renderer.line.strokeWidth = 2;
        categoryAxis.renderer.line.stroke = am4core.color("#3787ac");

        var label = categoryAxis.renderer.labels.template;
        label.truncate = true;
        label.maxWidth = 120;
        label.tooltipText = "{category}";

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Total Number";
        valueAxis.adjustLabelPosition = true;
        // valueAxis.renderer.baseGrid.disabled = true;

        var series = chart.series.push(new am4charts.LineSeries());
        series.name = "Violations";
        series.dataFields.valueY = "value";
        series.dataFields.categoryX = "category";

        var bullet = series.bullets.push(new am4charts.CircleBullet());
        bullet.circle.strokeWidth = 2;
        bullet.circle.radius = 4;
        bullet.circle.fill = am4core.color("#fff");
        bullet.circle.tooltipText = "{valueY}"
        // bullet.circle.showTooltipOn = "always";

        var bullethover = bullet.states.create("hover");
        bullethover.properties.scale = 1.3;

        // chart.cursor = new am4charts.XYCursor();

        // Create a horizontal scrollbar with preview and place it underneath the date axis
        // chart.scrollbarX = new am4charts.XYChartScrollbar();
        // chart.scrollbarX.series.push(series);
        // chart.scrollbarX.parent = chart.bottomAxesContainer;
    }
</script>
@endsection
