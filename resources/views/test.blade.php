@extends('layouts.admin')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Testing</h6>
    </div>
    <div class="card-body ">
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-success" id="truncate">Truncate Tables</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $('#truncate').click(function (){
        $.get({
            url: "{{route('test.truncate')}}",
            beforeSend: () => {
                $(this).attr('disabled', true);
            },
            complete: (d) => {
                console.log(d);
                $(this).attr('disabled', false);
                alertify.success(d.responseJSON['message']);
            }
        });
    });
</script>

@endsection
