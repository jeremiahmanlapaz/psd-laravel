@extends('layouts.admin')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Announcement / </h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <form id="form" method="post" action="{{ route('announcement.send') }}">
                    @csrf
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title"
                            id="title">
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="body">Body</label>
                        <textarea class="form-control @error('body') is-invalid @enderror" name="body" id="body" rows="3"></textarea>
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <button class="btn btn-success" type="submit">Send Announcement</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
{{-- <script>
    $('#form').submit(function(e) {
            e.preventDefault();
            console.log('submit');
            $.ajax({
                type: 'post',
                url: $(this).data('url'),
                data: {
                    title: $('#title').val(),
                    body: $('#body').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                complete: (response) => {
                    console.log(response);
                    alert(response.responseText);
                    $('#title').val("");
                    $('#body').val("");
                },
            });
        });
</script> --}}
@endsection