@extends('layouts.home')

@section('content')
<div class="row m-2">
    <div class="col-md-4 vh-100">
        <div class="card o-hidden border-0 shadow-lg" style="height: 100%;">
            <div class="card-body">
                <div class="mx-auto">
                    <div class="px-3 text-center">
                        <h3 id="date"></h3>
                        <h1 id="time"></h1>
                    </div>
                    <div class="py-2">
                        <img src="{{ asset('images/person.png') }}" class="img-fluid" alt="Responsive image">
                    </div>
                    <div class="input-group my-1 mx-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span><i class="fas fa-barcode"></i> </span>
                                <input type="radio" name="inputType" id="inputType" value="scanner"
                                    aria-label="Input ID Number" class="mx-2" checked>
                            </div>
                        </div>
                        <input type="text" class="form-control form-control-lg scanner" aria-label="Input ID Number"
                            name="scanner" id="inputScanner" autofocus placeholder="KXXXXXXX">
                    </div>
                    <div class="input-group my-1 mx-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span><i class="fas fa-keyboard"></i> </span>
                                <input type="radio" name="inputType" value="keyboard" aria-label="Input ID Number"
                                    class="mx-2">
                            </div>
                        </div>
                        <input type="text" class="form-control form-control-lg keyboard" aria-label="Input ID Number"
                            name="keyboard" id="keyboard" disabled>
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary btn-success keyboard" type="button" name="submit"
                                id="submit" disabled>Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 vh-100">
        <div class="card o-hidden border-0 shadow-lg " style="height: 100%;">
            <div class="card-body p-2">
                <div class="px-3">
                    <table class="table table-hover" id="timeTable">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        let table = $('#timeTable').DataTable({
            ajax: {
                url: '/api/logs/all?limit=100',
                dataSrc: '',
            },
            columnDefs: [
                {orderable: false, targets: "_all"}
            ],
            columns:[
                {data: 'id_number', title: 'ID NUMBER'},
                {data: null, render: (d) => d.first_name + ' ' + d.last_name, title: 'Name'},
                {
                    data: 'time_in',
                    title: 'TIME IN',
                    render: (d) => {
                        return d != null ? moment(d).format('YYYY-MM-DD hh:mm'):'';
                    }
                },
                {
                    data: 'time_out',
                    title: 'TIME OUT',
                    render: (d) => {
                        return d != null ? moment(d).format('YYYY-MM-DD hh:mm'):'';
                    }
                }
            ],
            order: false,
            dom: ''
        });

        const interval = 1000;
        initClock(interval);

        $('#inputScanner').change(function () {
            let val = $(this).val();
            Log(val);
            table.ajax.reload();
        });

        $('#submit').click(function() {
            let val = $('[name=keyboard]').val();
            Log(val);
            table.ajax.reload();
        })
    });

    $('#inputType').on('change', function() {
        let val = $(this).val();
        checkInput(val);
    });

    function Log(id_number){
        let url = "/api/logs/log";
        $.ajax({
            url: url,
            type: 'post',
            data: {"id_number": id_number},
            beforeSend: function () {
                disableInputs();
            },
            complete: (d, s) => {
                swal_alert(s, d.responseJSON.message)
            }
        });
    }

    function disableInputs() {
        $('.keyboard, .scanner').attr('disabled', true);
        setTimeout(() => {
            $('.keyboard, .scanner').attr('disabled', false);
            let val = $('#inputType:checked').val();
            checkInput(val);
        }, 2000);
    }

    function checkInput(val) {
        if(val == 'scanner'){
            $('.keyboard').attr('disabled', true);
            $('.scanner').attr('disabled', false).focus().val('');
        }else {
            $('.keyboard').attr('disabled', false);
            $('.scanner').attr('disabled', true);
            $('#keyboard').focus().val('');
        }
    }

    function initClock(interval) {
        $('#date').text(moment().format('dddd, MMMM D, YYYY'))
        setInterval(() => {
            $('#time').text("" + moment().format('hh:mm:ss A'));
        }, interval);
    }

    var swal_alert = (icon = 'success', title, text) => {
        swal.fire({
            icon: icon,
            title: title,
            text: text,
            timer: icon === 'error' ? 5000 : 3000,
        });
    }
</script>
@endsection