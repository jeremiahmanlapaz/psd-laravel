@extends('layouts.admin')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Students / Violations </h6>
    </div>
    <div class="card-body ">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-sm nowrap" style="width: 100%" id="table">
                    <thead></thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    $(document).ready(function () {
        let table = $('#table').DataTable({
            scrollX: true,
            "autoWidth": false,
            ajax: {
                url: "/api/violations/all",
                dataSrc: ''
            }, 
            columns: [
                {data: 'id_number', title: 'ID Number'},
                {data: 'title', title: 'Violation'},
                {data: null, title: 'Sanction', render: (d) => {
                    // console.log(d.type);
                        if(d['type'] == "Minor") {
                            return `<p class="badge badge-pill badge-warning">${d.sanction}</p>`;
                        }else{
                            return `<p class="badge badge-pill badge-danger">${d.sanction}</p>`;
                        }
                    }
                },
                {data: 'expired_at', title: 'Expiry Date'},
                {data: 'status', title: 'Status',
                    render: (d) => {
                        if(d == "pending") {
                            return `<p class="badge badge-pill badge-warning">${d}</p>`;
                        }else{
                            return `<p class="badge badge-pill badge-success">${d}</p>`;
                        }
                    }
                },
                {data: 'cleared_at', title: 'Cleared Date'},

            ],
            dom: 'Bfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-file-csv"></i> Save as CSV',
                    extend: 'csv',
                    className: 'btn btn-success mx-1 rounded-pill',
                    title: 'Report List'
                },
                {
                    text: '<i class="fas fa-file-pdf"></i> Save as PDF',
                    extend: 'pdf',
                    className: 'btn btn-success mx-1 rounded-pill',
                    title: 'Report List'
                },
            ]
        });
    });
</script>
@endsection