@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Students / Violations / {{ ucfirst($id_number)}} </h6>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="table"></table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        let url = "{{route('students.violations.get', ['id_number' => ':id_number'])}}";
        let id_number = "{{$id_number}}";
        let table = $('#table').DataTable({
            ajax: {
                url: url.replace(':id_number', id_number),
                dataSrc: ''
            },
            columns: [
                {data: 'title', title: 'Title'},
                {
                    data: 'type', title: 'Type', render: function (data) {
                        if(data == "Minor") {
                            return `<p class="badge badge-pill badge-warning">${data}</p>`;
                        } else {
                            return `<p class="badge badge-pill badge-danger">${data}</p>`;
                        }
                    }
                },
                {data: 'penalty', title: 'Penalty/Sanction'},
                {data: 'expired_at', title: 'Expires Date'},
                {data: 'cleared_at', title: 'Cleared Date'},
                {
                    data: null, title: 'Action', render: function (data) {
                        return `<button type="button" class="btn btn-success btn-sm" data-id=${data.id} name="potangina"><i class="fas fa-check"></i> Clear</button>`;
                    }
                }
            ],
            dom: 'lBfrtip',
            // buttons: []
        });

        $(document).on('click', 'button[name=potangina]', function () {
            let id = $(this).data('id');
            console.log(id);
            let url = "{{route('students.violations.clear', ['id' => ':id'])}}";
            $.ajax({
                url: url.replace(':id', id),
                type: 'get',
                success: function (d, s, j) {
                    alertify.success(j.responseJSON.message);
                },
                error: function (j, s, e) {
                    console.log(j);
                },
                complete: function () {
                    table.ajax.reload();
                }
            });
        }); 
    });
</script>
@endsection