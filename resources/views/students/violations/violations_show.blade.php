@extends('layouts.admin')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Students / <a href="{{ route('violations.index') }}">Violations</a> / {{$id_number}} </h6>
    </div>
    <div class="card-body ">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-sm nowrap" style="width: 100%" id="table">
                    <thead></thead>
                </table>
            </div>
        </div>
    </div>
</div>

{{-- Violation create modal --}}
@include('students.violations.modals.violation_create_modal')

{{-- Violation clearance modal --}}
@include('students.violations.modals.violation_clearance_modal')

@endsection

@section('js')
<script>
    $('#violation_id').select2({theme: "classic"});
        var table = $('#table').DataTable({
            ajax: {
                url: "/api/violations/get/{{ $id_number }}",
                dataSrc: ''
            },
            fixedColumns: {
                leftColumns:0,
                rightColumns: 1
            },
            columns: [
                {data: 'id_number', title: 'ID Number'},
                {
                    data: 'type', title: 'Violation type', className: 'text-center',
                    render: function (data) {
                        if(data == 'Minor') {
                            return `<p class="badge badge-pill badge-warning">${data}</p>`;
                        } else{
                            return `<p class="badge badge-pill badge-danger">${data}</p>`;
                        }
                    }
                },
                {data: 'title', title: 'Title'},
                {
                    data: 'sanction', title: 'Sanction', render: d => {
                        if(d == "Suspension" || d == "Expelled") {
                            return `<p class="badge badge-danger badge-pill rounded-pill">${d}</p>`;
                        }else{
                            return `<p class="badge badge-warning badge-pill rounded-pill">${d}</p>`;
                        }
                    }
                },
                {
                    data: 'status', title: 'Violation status', render: (d) => {
                        if (d == 'pending') {
                            return `<p class="badge badge-warning rounded-pill px-3">${d}</p>`;
                        }else if (d == 'expired_at') {
                            return `<p class="badge badge-danger rounded-pill px-3">${d}</p>`;
                        } else {
                            return `<p class="badge badge-success rounded-pill px-3">${d}</p>`;
                        }
                    }
                },
                {
                    data: 'created_at', title: 'Violation Date', render: (d) => {
                        let date = moment(new Date(d)).format("MMMM DD, YYYY");
                        return `${date}`;
                    }
                },
                {
                    data: 'expired_at', title: 'Expiry Date', render: (d) => {
                        if(d != null){
                            let date = moment(new Date(d)).format("MMMM DD, YYYY");
                            return `${date}`;
                        }else{
                            return '';
                        }
                    }
                },
                {
                    data: null, name: 'action',title: 'Action', render: (d) => {
                        return d.status == 'cleared' ? '' : `<button type="button" name="clear" class="btn btn-success btn-sm mx-1" data-toggle="modal" data-target="#clearance" data-id=${d.id}> <i class="fas fa-check"></i> Clear</button>`;
                    }
                }
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fa fa-plus"></i> Add Violation',
                    className: 'btn btn-warning btn-sm rounded-pill',
                    action: function () {
                        $('#add').modal('show');
                    }
                },
            ],
            scrollX: true
        });

        $('#add').on('show.bs.modal', function (e) {
            let btn = $(e.relatedTarget);
            let id = btn.data('id-number');
            let val_id = $('#violation_id').val();
            getSanction(val_id);
        });

        $('#violation_id').change(function() {
            let id = $(this).val();
            getSanction(id);
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            let data = $(this).serialize();
            let url = "{{route('violations.create', ['id_number' => ':id'])}}";
            $.ajax({
                url: url.replace(':id', id),
                type: 'post',
                data: data,
                beforeSend: function () {
                    $('#add').modal('hide');
                },
                complete: function (j, s) {
                    if(j.status >= 200 && j.status < 400) {
                        alertify.success(j.responseJSON.message)
                    }else {
                        alertify.error(j.responseJSON.message)
                    }
                    table.ajax.reload();
                }
            });
        });

        let clearance_id;
        
        $('#clearance').on('show.bs.modal', function (e) {
            clearance_id = $(e.relatedTarget).data('id');

        });

        $('#confirm_clearance').click(function(e){
            // console.log(clearance_id);
            clearViolation(clearance_id);
            $('#clearance').modal('toggle');
        });

        function clearViolation(id){
            // let id = $(e.target).data('id');
            $.ajax({
                url: "/api/violations/clear",
                type: 'post',
                data: {id: id},
                complete: (j, s) => {
                    if(j.statusText = "OK"){
                        alertify.success(j.responseJSON.message);
                    }
                    table.ajax.reload();
                }
            });
        }

        function getSanction(id) {
            $.ajax({
                url: "/api/violation/get/{{ $id_number }}",
                type: 'get',
                complete: function(j, s) {
                    $('#sanction').children().remove();
                    if(j.statusText == "OK") {
                        j.responseJSON.forEach(e => {
                            if(e){
                                $('#sanction').append($('<option>').text(e).val(e));
                            }
                        });
                    }
                }
            });
        }
</script>
@if (Auth::user()->role == 1)
<script>
    table.on('draw', function () {
        table.column(7).visible(false);
    });
</script>
@endif
@endsection