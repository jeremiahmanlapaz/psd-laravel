@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Students / Create</h6>
            </div>
            <div class="card-body">
                <form method="post">
                    @csrf
                    <div class="form-group">
                        <label for="id_number">ID Number</label>
                        <input type="text" name="id_number" id="id_number"
                            class="form-control @error('id_number') is-invalid @enderror" value="{{old('id_number')}}">
                        <small>For new violation only.</small>
                        @error('id_number')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="violation_id">Violation</label>
                        <select name="violation_id" id="violation_id" class="form-control">
                            @foreach ($violations as $violation)
                            <option value="{{$violation->id}}"
                                {{old('violation_id') == $violation->id?'selected':null}}>
                                {{$violation->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-warning"><i class="fas fa-save"></i> Register Violation to
                            Student</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function () {

    });
</script>
@endsection