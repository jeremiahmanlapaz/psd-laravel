<!-- Add violation modal -->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="addModaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModaLabel">Add Violation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{-- <form method="post" action="{{route('students.violations.create')}}"> --}}
            <form method="POST" id="form">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="id_number">ID Number</label>
                        <input type="text" name="id_number" id="id_number" value="{{$id_number}}" readonly
                            class="form-control @error('id_number') is-invalid @enderror">
                        @error('id_number')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="violation_id">Violation</label>
                        <select name="violation_id" id="violation_id" class="form-control" style="width: 100%">
                            @foreach ($violations as $violation)
                            <option value="{{$violation->id}}">{{$violation->title}}</option>
                            @endforeach
                        </select>
                        @error('violation_id')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="sanction">Sanction</label>
                        <select name="sanction" id="sanction"
                            class="form-control @error('sanction') is-invalid @enderror">
                        </select>
                        @error('sanction')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-warning">
                        <i class="fas fa-save"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>