@extends('layouts.admin')

@section('content')
<!-- Basic Card Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">
            Students / <a href="{{route('profiles.index')}}">Profile</a> / {{$profile->id_number}}
        </h6>
    </div>
    <div class="card-body">

        <div class="container py-4 my-2">
            <div class="row">
                <div class="col-md-4 pr-md-5">
                    <img class="w-100 rounded border" src="{{ asset('storage/'.$profile->profile_picture)}}"
                        alt="Profile Picture" title="{{$profile->first_name}}" />
                    <div class="pt-4 mt-2">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="d-flex align-items-center">
                        <h2 class="font-weight-bold m-0">
                            {{$profile->first_name.' '.$profile->middle_name.' '.$profile->last_name}}
                        </h2>
                    </div>
                    <section class="mt-4">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                    aria-controls="home" aria-selected="true">
                                    About
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content py-4" id="myTabContent">
                            <div class="tab-pane py-3 fade show active" id="home" role="tabpanel"
                                aria-labelledby="home-tab">
                                <h6 class="text-uppercase font-weight-light text-secondary">
                                    Contact Information
                                </h6>
                                <dl class="row mt-4 mb-4 pb-3">
                                    <dt class="col-sm-3">Phone</dt>
                                    <dd class="col-sm-9">{{$profile->contact_number}}</dd>

                                    <dt class="col-sm-3">Email address</dt>
                                    <dd class="col-sm-9">{{$profile->umak_email}}</dd>
                                </dl>

                                <h6 class="text-uppercase font-weight-light text-secondary">
                                    Educational Background
                                </h6>
                                <dl class="row mt-4 mb-4 pb-3">
                                    <dt class="col-sm-3">College</dt>
                                    <dd class="col-sm-9">{{$profile->college}}</dd>

                                    <dt class="col-sm-3">Course</dt>
                                    <dd class="col-sm-9">{{$profile->course}}</dd>
                                </dl>
                            </div>
                        </div>
                    </section>
                    <div class="center">
                        <a href="{{ route('profiles.index') }}" class="btn btn-success btn-sm">Return</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection