@extends('layouts.admin')

@section('content')
<!-- Basic Card Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Students / Profiles</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-sm nowrap" id="table" style="width:100%"></table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importModalLabel">Import File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('profiles.import') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFile" name="file" required
                            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="SMSModal" tabindex="-1" role="dialog" aria-labelledby="SMSModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="SMSModalLabel">SMS Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="SMSForm" action="{{ route('globe.send') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="address">Receiver Number: </label>
                        <input type="text" name="address" id="address" class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" class="form-control"></textarea>
                        <small class="form-text text-muted">160 Characters only</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        let table = $('#table').DataTable({
            scrollX: true,
            ajax: {
                url: "/api/profiles/all",
                dataSrc: ''
            },
            fixedColumns: {
                leftColumns:0,
                rightColumns: 1
            },
            columns: [
                {data: 'id_number', title: 'Student Number'},
                {data: 'first_name', title: 'First Name'},
                {data: 'middle_name', title: 'Middle Name'},
                {data: 'last_name', title: 'Last Name'},
                {data: 'college', title: 'College'},
                // {data: 'course', title: 'Course'},
                {data: 'year_level', title: 'Year Level'},
                // {data: 'contact_number', title: 'Contact Number'},
                {data: 'umak_email', title: 'Umak Email'},
                // {
                //     data: 'status', title: 'Status', render: d => {
                //         return `<p class="badge badge-pill badge-${d == 'Expelled'?'danger':'success'} rounded-pill">${d}</p>`;
                //     }
                // },
                {
                    data: null, title: 'Action', render: d => {
                    let show = '{{route("profiles.show", ":id")}}';
                    let v    = '{{route("violations.show", ":id")}}';
                    let l    = '{{route("logs.show", ":id")}}'
                    return `<a class="btn btn-info btn-sm rounded-pill ml-1" href=${show.replace(":id", d.id_number)}><i class="fas fa-eye fa-sm"></i> View</a>` +
                    `<a class="btn btn-info btn-sm rounded-pill ml-1" href=${v.replace(":id", d.id_number)}><i class="fas fa-list fa-sm"></i> Violations</a>` +
                    `<a class="btn btn-success btn-sm rounded-pill ml-1" href=${l.replace(":id", d.id_number)}> Logs</a>`
                    // `<a class="btn btn-warning btn-sm rounded-pill ml-1" href="#"><i class="fas fa-pencil-alt"></i> Edit</a>` +
                    // `<button type="button" class="btn btn-success btn-sm rounded-pill ml-1" data-number=${d.contact_number} data-toggle="modal" data-target="#SMSModal"><i class="fas fa-envelope"></i> SMS</button>`;
                    }
                },
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-eye"></i> All',
                    className: 'btn btn-sm btn-warning mx-1 rounded-pill',
                    action: function (){
                        table.ajax.url("/api/profiles/all");
                        table.ajax.reload();
                    }
                },
                {
                    text: '<i class="fas fa-eye"></i> With Violations',
                    className: 'btn btn-sm btn-warning mx-1 rounded-pill',
                    action: function (){
                        table.ajax.url("/api/profiles/violations");
                        table.ajax.reload();
                    }
                },
                {
                    text: '<i class="fas fa-plus"></i> Add Student',
                    className: 'btn btn-sm btn-success mx-1 rounded-pill',
                    action: function (){
                        window.location.href = "{{route('profiles.create')}}"
                    }
                },
                {
                    text: '<i class="fas fa-file-import"></i> Import',
                    className: 'btn btn-sm btn-success mx-1 rounded-pill',
                    action: function () {
                        $('#importModal').modal('show');
                    }
                },
                // Printing
                // {
                //     text: '<i class="fas fa-print"></i> Print',
                //     extend: 'print',
                //     className: 'btn btn-success mx-1 rounded-pill'
                // },
                // {
                //     text: '<i class="fas fa-file-csv"></i> CSV',
                //     extend: 'csv',
                //     className: 'btn btn-success mx-1 rounded-pill'
                // },
                // {
                //     text: '<i class="fas fa-file-pdf"></i> PDF',
                //     extend: 'pdf',
                //     className: 'btn btn-success mx-1 rounded-pill'
                // },
            ],
        });

        $('#SMSModal').on('show.bs.modal', function (e) {
            let number = $(e.relatedTarget).data('number');
            $('#address').val(number);
        }).on('hide.bs.modal', function (e) {
            $('#address').val('');
        });

        $('#SMSForm').on('submit', function (e) {
            e.preventDefault();
            let formData = $(this).serializeArray();
            $.ajax({
                url: "{{route('globe.send')}}",
                type: "post",
                data: formData,
                complete: (j, s) => {
                    console.log({"data": j, "status": s});
                    if(s == "success"){
                        alertify.success(j.responseJSON.message);
                    }else {
                        alertify.error(j.responseJSON.message);
                    }
                }
            });
            $('#SMSModal').modal('hide');
        });
    });
</script>
@endsection
