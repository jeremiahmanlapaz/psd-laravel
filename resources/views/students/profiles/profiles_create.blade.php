@extends('layouts.admin')

@section('content')
<!-- Basic Card Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Students / Create</h6>
    </div>
    <div class="card-body">
        <form action="{{ route('profiles.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row justify-content-center align-items-center">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="id_number">ID Number</label>
                        <input type="text" name="id_number" id="id_number"
                            class="form-control @error('id_number') is-invalid @enderror" value="{{old('id_number')}}">
                        @error('id_number')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Profile Picture</label>
                        <div class="custom-file">
                            <input type="file"
                                class="custom-file-input file @error('profile_picture') is-invalid @enderror"
                                id="customFile" accept="image/*" name="profile_picture">
                            @error('profile_picture')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="https://via.placeholder.com/400" class="img-fluid" id="preview" alt="Image Preview"
                        style="width: 400px; height: auto;">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" name="first_name" id="first_name"
                            class="form-control @error('first_name') is-invalid @enderror"
                            value="{{old('first_name')}}">
                        @error('first_name')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="middle_name">Middle Name</label>
                        <input type="text" name="middle_name" id="middle_name"
                            class="form-control @error('middle_name') is-invalid @enderror"
                            value="{{old('middle_name')}}">
                        @error('middle_name')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" name="last_name" id="last_name"
                            class="form-control @error('last_name') is-invalid @enderror" value="{{old('last_name')}}">
                        @error('last_name')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="college">College</label>
                        <select name="college" id="college" class="form-control form-control-md">
                        </select>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="course">Course</label>
                        <select name="course" id="course" class="form-control form-control-md">
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="year_level">Year Level</label>
                        <select class="form-control" name="year_level" id="year_level">
                            <option value="1" {{old('year_level') == '1'? 'selected':null}}>1</option>
                            <option value="2" {{old('year_level') == '2'? 'selected':null}}>2</option>
                            <option value="3" {{old('year_level') == '3'? 'selected':null}}>3</option>
                            <option value="4" {{old('year_level') == '4'? 'selected':null}}>4</option>
                            <option value="5" {{old('year_level') == '5'? 'selected':null}}>5</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="contact_number">Contact Number</label>
                        <input type="number" name="contact_number" id="contact_number"
                            class="form-control @error('contact_number') is-invalid @enderror"
                            value="{{old('contact_number')}}">
                        @error('contact_number')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="guardian_contact_number">Guardian Contact Number</label>
                        <input type="number" name="guardian_contact_number" id="guardian_contact_number"
                            class="form-control @error('guardian_contact_number') is-invalid @enderror"
                            value="{{old('guardian_contact_number')}}">
                        @error('guardian_contact_number')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="umak_email">Umak Email</label>
                        <input type="text" name="umak_email" id="umak_email"
                            class="form-control @error('umak_email') is-invalid @enderror"
                            value="{{old('umak_email')}}">
                        @error('umak_email')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function() {

        $('#college').ready(getColleges()).change(function () {
            let val = $(this).val();
            getCourses(val);
        });
        $('input[type="file"]').change(function(e) {
            var fileName = e.target.files[0].name;

            var reader = new FileReader();
            reader.onload = function(e) {
                // get loaded data and render thumbnail.
                document.getElementById("preview").src = e.target.result;
            };
            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });
    });

    function getColleges(){
        $.get({
            url: "{{route('colleges.all')}}",
            success: function (data) {
                $.each(data, function (index, val) {
                    $('#college').append($('<option>').text(val.name).val(val.code));
                });
            }, 
            complete: function () {
                getCourses($('#college').val());
            }
        });
    }

    function getCourses(code) {
        let url = "{{route('api.courses.get', ['code' => ':code'])}}"; 
        $.get({
            url: url.replace(':code', code),
            success: function (data) {
                let course = $('#course');
                course.children().remove();
                $.each(data, (index, val) => {
                    course.append($('<option>').text(val.name).val(val.name));
                });
            }
        });
    }
</script>
@endsection