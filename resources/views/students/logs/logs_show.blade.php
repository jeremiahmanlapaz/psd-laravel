@extends('layouts.admin')

@section('content')

<!-- Basic Card Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Students / Logs / {{$id_number}}</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-sm nowrap" id="table" style="width:100%">
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    $(document).ready(function(){
        let u = "/api/logs/get/:id";
        let table = $('#table').DataTable({
            ajax: {
                url: u.replace(':id', '{{$id_number}}'),
                dataSrc: ''
            },
            columns: [
                {data: 'id_number', title: 'Student Number'},
                {data: 'time_in', title: 'Time In'},
                {data: 'time_out', title: 'Time Out'},
            ]
        });
    });
</script>
@endsection
