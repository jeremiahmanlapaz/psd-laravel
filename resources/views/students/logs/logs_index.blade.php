@extends('layouts.admin')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Logs</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table" id="table"></table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        let table = $('#table').DataTable({
            ajax: {
                url: "/api/logs/all",
                dataSrc: ''
            },
            columns: [
                {data: 'id_number', title: 'ID Number'},
                {data: 'time_in', title: 'Time In'},
                {data: 'time_out', title: 'Time Out'},
            ],
            dom: 'Bfrtip',
            buttons: [
                'pdf', 'csv'
            ]
        });
    });
</script>
@endsection
