@extends('layouts.admin')

@section('content')
<div class="row">

    <!-- Top Cards -->
    <div class="mb-4 mx-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Number of Students</div>
                        <div class="h5 text-center mb-0 font-weight-bold text-gray-800">{{$total_students}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-4 mx-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Violations this Month
                        </div>
                        <div class="h5 text-center mb-0 font-weight-bold text-gray-800">{{$violation_month}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="mb-4 mx-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pending Violations
                        </div>
                        <div class="h5 text-center mb-0 font-weight-bold text-gray-800">{{$pending}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-times fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-4 mx-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Cleared Violations
                        </div>
                        <div class="h5 text-center mb-0 font-weight-bold text-gray-800">{{$clear}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-times fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Basic Card Example -->
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Total Violation for academic year 2019 - 2020</h6>
            </div>
            <div class="card-body">
                <div id="lineChart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mb-3">
        <div class="row">
            <div class="col-md-3">
                <label>Month</label>
                <select class="form-control form-control-sm" id="chart_month">
                    <option value="">ALL</option>
                    @for ($i = 1; $i <= 12; $i++) <option value="{{$i}}">
                        {{ Carbon\Carbon::create(2020,$i)->format('F')}}
                        </option>
                        @endfor
                </select>
            </div>
            <div class="col-md-3">
                <label>College</label>
                <select class="form-control form-control-sm" id="chart_college">
                    <option value="">ALL</option>
                    @foreach ($colleges as $college)
                    <option value="{{$college->code}}">{{$college->code}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                <label>Year Level</label>
                <select class="form-control form-control-sm" id="chart_level">
                    <option value="">ALL</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Violation by Type</h6>
            </div>
            <div class="card-body">
                <div id="pieChart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Violation by Gender</h6>
            </div>
            <div class="card-body">
                <div id="pieChart2" style="height: 400px;"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        let pieChart = initPieChart("pieChart");
        let pieChart2 = initPieChart("pieChart2");
        let lineChart = initLineChart("lineChart");
        let url = "{{route('analytics.total.type')}}";
        let url2 = "{{route('analytics.total.gender')}}";
        let url3 = "{{route('analytics.total.title')}}";
        setPiechartData(pieChart, url);
        setPiechartData(pieChart2, url2);
        setLineChartData(lineChart, url3);

        $('#chart_month, #chart_college, #chart_level').on('change', function () {
            let college = $('#chart_college').val();
            let month = $('#chart_month').val();
            let year_level = $('#chart_level').val();
            setPiechartData(pieChart, url, month, college, year_level);
            setPiechartData(pieChart2, url2, month, college, year_level);
            // setLineChartData(lineChart, url3, month, college, year_level);
        });
    });

    function setLineChartData(chart, url = "", m = "", c = "", v = "", yl = "") {
        $.ajax({
            url: url,
            dataType: 'JSON',
            data: {month: m, college: c, violation: v, year_level: yl},
            complete: (j, s) => {
                if(j.status >= 200 || j.status < 400) {
                    chart.data = j.responseJSON ?? 'EMPTY';
                }
            }
        });
    }

    function setPiechartData(chart, url = "", m = "", c = "", v = "", yl = "",) {
        $.ajax({
            url: url,
            dataType: 'JSON',
            data: {month: m, college: c, violation: v, year_level: yl},
            complete: (j, s) => {
                if(j.status >= 200 || j.status < 400) {
                    chart.data = j.responseJSON ?? 'EMPTY';
                }
            }
        });
    }

    function initPieChart(element) {
        let chart = am4core.create(element, am4charts.PieChart);
        // chart.data = data;
        chart.exporting.menu = new am4core.ExportMenu();
        let pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "value";
        pieSeries.dataFields.category = "category";
        return chart;
    }

    function initLineChart (element) {
        let chart = am4core.create(element, am4charts.XYChart);
        // chart.data = data;
        chart.exporting.menu = new am4core.ExportMenu();

        let title = chart.titles.create();
        // title.text = chartTitle;

        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        // categoryAxis.title.text = Title;
        categoryAxis.renderer.labels.template.disabled = true;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Number";

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        // series.name = "Violation by Gender";
        series.dataFields.valueY = "value";
        series.dataFields.categoryX = "category";
        let category = "{category}";
        series.columns.template.tooltipText =  `{categoryX}:{valueY}`;

        return chart;
    }
</script>
@endsection