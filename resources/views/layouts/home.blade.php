<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="PSD">
        <meta name="author" content="Maya">
        <title>PSD</title>
        <link rel="icon" type="image/png" href="{{ asset('images/PSD_Logo.png') }}" />
        <!-- Custom fonts for this template-->
        <link href="{{ asset('vendors/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

        <!-- Custom Plugin -->
        <link rel="stylesheet" href="{{ asset('vendors/DataTables/datatables.min.css') }}">

    </head>
    <style>
        .h-100 {
            min-height: 100vh;
        }
    </style>

    <body class="bg-primary">
        <div class="container-fluid">
            <div class="justify-content-center">
                @yield('content')
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-4/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('js/moment.js') }}"></script>
        <!-- Core plugin JavaScript-->
        <script src="{{ asset('vendors/jquery-easing/jquery.easing.min.js') }}"></script>

        <!-- Custom scripts for all pages-->
        <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

        <!-- Custom Plugin JS -->
        <script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        @yield('js')
    </body>

</html>