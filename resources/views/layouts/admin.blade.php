<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="PSD">
        <meta name="author" content="Maya">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{config('app.name', 'PSD')}} </title>
        <link rel="icon" type="image/png" href="{{ asset('images/PSD_Logo.png') }}" />

        <!-- Custom fonts for this template-->
        <link href="{{ asset('vendors/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="{{ asset('vendors/PACE/themes/blue/pace-theme-corner-indicator.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/alertify/css/alertify.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/alertify/css/themes/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/DataTables/datatables.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/select2/css/select2.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/select2/css/select2-bootstrap.css') }}">

        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

    </head>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center"
                    href="{{ route('admin.home') }}">
                    <div class="sidebar-brand-icon">
                        {{-- <i class="fas fa-laugh-wink"></i> --}}
                        <img src="{{ asset('images/PSD_logo.png') }}" width="64" height="64" />
                    </div>
                    <div class="sidebar-brand-text mx-3">
                        {{ config('app.name', 'PSD')}}
                        <sup>{{now()->format('Y')}}</sup>
                    </div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                @if(Auth::user()->role == "0" || "admin")
                <!-- Nav Item - Dashboard -->
                <li class="nav-item {{request()->is('admin/dashboard*') ? 'active':null }}">
                    <a class="nav-link" href="{{ route('admin.home') }}">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">
                @endif
                <div class="sidebar-heading">
                    Students
                </div>

                <li class="nav-item {{request()->is('admin/students/logs*') ? 'active':null }}">
                    <a class="nav-link" href="{{ route('logs.index') }}">
                        <i class="fas fa-fw fa-calendar-alt"></i>
                        <span>Logs</span>
                    </a>
                </li>

                <li class="nav-item {{request()->is('admin/students/profiles*') ? 'active':null }}">
                    <a class="nav-link" href="{{ route('profiles.index') }}">
                        <i class="fas fa-fw fa-users"></i>
                        <span>Profiles</span>
                    </a>
                </li>

                <li class="nav-item {{request()->is('admin/students/violations*') ? 'active':null }}">
                    <a class="nav-link" href="{{ route('violations.index') }}">
                        <i class="fas fa-users-slash"></i>
                        <span>Violations</span>
                    </a>
                </li>

                @if (Auth::user()->role == "0" || "admin")
                <hr class="sidebar-divider d-none d-md-block">

                <div class="sidebar-heading">
                    CMS
                </div>

                <li class="nav-item {{request()->is('admin/announcement*') ? 'active':null }}">
                    <a class="nav-link" href="{{ route('announcement') }}">
                        <i class="fas fa-fw fa-chart-bar"></i>
                        <span>Announcement</span>
                    </a>
                </li>

                {{-- <li class="nav-item {{request()->is('admin/analytics*') ? 'active':null }}">
                    <a class="nav-link" href="{{ route('analytics') }}">
                        <i class="fas fa-fw fa-chart-bar"></i>
                        <span>Analytic</span>
                    </a>
                </li> --}}

                <li class="nav-item {{request()->is('admin/analytics*') ? 'active' : null }}">
                    <a class="nav-link collapsed" href="#"
                        data-toggle="collapse" data-target="#collapseAnalytics" aria-expanded="true"
                        aria-controls="collapseAnalytics">
                        <i class="fas fa-fw fa-file-excel"></i>
                        <span>Analytics</span>
                    </a>
                    <div id="collapseAnalytics" class="collapse" aria-labelledby="Analytics" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Analytics: </h6>
                            <a class="collapse-item {{request()->is('admin/analytics/index') ? 'active':null }}"
                                href="{{ route('analytics.index') }}">
                                <span>Charts</span>
                            </a>
                            <a class="collapse-item {{request()->is('admin/analytics/cluster') ? 'active':null }}"
                                href="{{ route('analytics.cluster') }}">
                                <span>Clustering</span>
                            </a>
                        </div>
                    </div>
                </li>

                {{-- <li class="nav-item {{request()->is('admin/reports*') ? 'active':null }}">
                <a class="nav-link" href="{{ route('reports') }}">
                    <i class="fas fa-fw fa-file-invoice"></i>
                    <span>Reports</span></a>
                </li> --}}

                <li class="nav-item {{request()->is('admin/reports*') ? 'active' : null }}">
                    <a class="nav-link collapsed" href="#"
                        data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
                        aria-controls="collapseTwo">
                        <i class="fas fa-fw fa-file-excel"></i>
                        <span>Report</span>
                    </a>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Reports:</h6>
                            <a class="collapse-item {{request()->is('admin/reports/violations') ? 'active':null }}"
                                href="{{ route('reports.violations') }}">
                                <span>Violations</span>
                            </a>
                            <a class="collapse-item {{request()->is('admin/reports/logs') ? 'active':null }}"
                                href="{{ route('reports.logs') }}">
                                <span>Logs</span>
                            </a>
                        </div>
                    </div>
                </li>

                <li class="nav-item {{request()->is('admin/violations*') ? 'active':null }}">
                    <a class="nav-link" href="{{ route('violations') }}">
                        <i class="fas fa-fw fa-scroll"></i>
                        <span>Violation List</span>
                    </a>
                </li>

                <li class="nav-item {{request()->is('admin/colleges*') ? 'active':null }}">
                    <a class="nav-link" href="{{ route('colleges') }}">
                        <i class="fas fa-fw fa-university"></i>
                        <span>College</span>
                    </a>
                </li>

                <li class="nav-item {{request()->is('admin/test*') ? 'active':null }}">
                    <a class="nav-link" href="{{ route('test') }}">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Test</span>
                    </a>
                </li>
                @endif
                <!-- Sidebar Toggler (Sidebar) -->
                <div class="text-center d-none d-md-inline">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>

            </ul>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Navbar -->
                        <ul class="navbar-nav ml-auto">

                            <div class="topbar-divider d-none d-sm-block"></div>

                            <!-- Nav Item - User Information -->
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{Auth::user()->name}}
                                    </span>
                                    <img class="img-profile rounded-circle" src="{{ asset('images/PSD_logo.png') }}">
                                </a>
                                <!-- Dropdown - User Information -->
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                    aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                        data-toggle="modal" data-target="#logoutModal">
                                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                        {{ __('Logout') }}
                                    </a>

                                </div>
                            </li>
                        </ul>
                    </nav>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        @yield('content')

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>{{__('Copyright')}} &copy;{{ config('app.name', 'PSD').' '.now()->format('Y')}}</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('Ready to Leave?')}}</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{__('Select "Logout" below if you are ready to end your current session.')}}
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                            <button class="btn btn-primary" type="submit">{{__('Logout')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Core plugin JavaScript-->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/moment.js') }}"></script>
        <script src="{{ asset('vendors/jquery-easing/jquery.easing.min.js') }}"></script>

        <!-- Custom scripts for all pages -->
        <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

        <script src="{{ asset('js/bootstrap-4/bootstrap.min.js') }}"></script>

        <!-- Custom import Library -->
        <script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script>
        <script src="{{ asset('vendors/amcharts4/core.js') }}"></script>
        <script src="{{ asset('vendors/amcharts4/charts.js') }}"></script>
        <script src="{{ asset('vendors/amcharts4/themes/animated.js') }}"></script>
        <script src="{{ asset('vendors/amcharts4/plugins/sliceGrouper.js') }}"></script>
        <script src="{{ asset('vendors/PACE/pace.min.js') }}"></script>
        <script src="{{ asset('vendors/select2/js/select2.js') }}"></script>
        {{-- <script src="{{ asset('js/bs-custom-file-input.js') }}"></script> --}}
        <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>

        <script src="{{ asset('vendors/alertify/alertify.min.js') }}"></script>

        @if (session('message') && session('class'))
        @if (session('class') == 'success')
        <script>
            alertify.success("{{session('message')}}");
        </script>
        @elseif (session('class') == 'error')
        <script>
            alertify.error("{{session('message')}}");
        </script>
        @elseif (session('class' == 'info'))
        <script>
            alertify.notify("{{session('message')}}");
        </script>
        @else
        <script>
            alertify.message("{{session('message')}}");
        </script>
        @endif
        @endif

        <script>
            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
            });
            bsCustomFileInput.init()
        </script>

        @yield('js')
    </body>

</html>