@extends('layouts.admin')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Colleges / </h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered" id="table"></table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="collegeModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importModalLabel">Import Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('colleges.import.college')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFile" name="file" required
                            accept="application/json">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="courseModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importModalLabel">Import Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('colleges.import.course')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="importFile" name="file" required
                            accept="application/json">
                        <label class="custom-file-label" for="importFile">Choose file</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        let table = $('#table').DataTable({
            ajax: {
                url: "{{route('colleges.all')}}",
                dataSrc: ''
            },
            columns: [
                {data: 'name', title: 'College'},
                {data: 'code', title: 'Code'},
                {data: null, title: 'Action', render: (d) => {
                    let url = "{{route('colleges.course', ['code' => ':code'])}}";
                    return `<a href=${url.replace(':code', d.code)} class="btn btn-success btn-sm mx-1"><i class="fas fa-eye"></i> View Courses</a>`;
                }},
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    text: `<i class="fas fa-file-import"></i>Import Colleges`,
                    className: 'btn btn-success btn-sm rounded mx-1',
                    action: () => {
                        $('#collegeModal').modal('show');
                    }
                },
                {
                    text: `<i class="fas fa-file-import"></i>Import Courses`,
                    className: 'btn btn-success btn-sm rounded mx-1',
                    action: () => {
                        $('#courseModal').modal('show');
                    }
                }
            ]
        });
    });
</script>
@endsection