@extends('layouts.admin')

@section('content')

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Reports / Violations</h6>
    </div>
    <div class="card-body ">
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label for="year">Academic Year</label>
                    <select class="form-control" id="year">
                        <option value="">All</option>
                        @for ($i = 2020; $i > 2017; $i--) <option value="{{$i}}">{{($i - 1).'-'. $i}}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Month</label>
                    <select name="month" id="month" class="form-control">
                        <option value="0">All</option>
                        @for ($i = 1; $i <= 12; $i++) <option value="{{ $i }}">
                            {{ Carbon\Carbon::create(0,$i)->englishMonth }}</option>
                            @endfor
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Type</label>
                    <select name="type" id="type" class="form-control">
                        <option value="">All</option>
                        <option value="minor">Minor</option>
                        <option value="major">Major</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-sm" id="table" style="width:100%">
                    <thead>
                        <tr>
                            <th>Violation Date</th>
                            <th>ID Number</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Year Level</th>
                            <th>Gender</th>
                            <th>College</th>
                            <th>Course</th>
                            <th>Title</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    $(document).ready(function () {
        $('#year, #type, #month').change(function () {
            let year = $('#year').val();
            let type = $('#type').val();
            let month = $('#month').val();
            let url = decodeURIComponent("{{route('reports.violations.get')}}?year=:year&type=:type&month=:month");
            let newUrl = url.replace(':year', year).replace(':type', type).replace(':month', month);
            console.log(newUrl);
            table.ajax.url(newUrl);
            table.ajax.reload();
        });

        let table = $('#table').DataTable({
            ajax: {
                url: "{{route('reports.violations.get')}}",
                dataSrc: ''
            },
            order: [[0, 'desc']],
            columns: [
                {   type: 'date',
                    data: 'updated_at',
                    render(d){
                        let date = moment(d).format('L');
                        return date;
                    }
                },
                {data: 'id_number'},
                {data: 'first_name'},
                {data: 'last_name'},
                {data: 'year_level'},
                {data: 'gender'},
                {data: 'college'},
                {data: 'course'},
                
                {data: 'title'},

            ],
            dom: 'Bfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-file-csv"></i> Save as CSV',
                    extend: 'csv',
                    className: 'btn btn-success mx-1 rounded-pill',
                    title: 'Report List'
                },
                {
                    text: '<i class="fas fa-file-pdf"></i> Save as PDF',
                    extend: 'pdf',
                    className: 'btn btn-success mx-1 rounded-pill',
                    title: 'Report List'
                },
            ]
        });
    });
</script>
@endsection