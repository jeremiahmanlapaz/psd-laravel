@extends('layouts.admin')

@section('content')

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Reports / Violations</h6>
    </div>
    <div class="card-body ">
        <div class="row">
            <div class="col-md-8">
                <div class="form-inline">
                    <label class="mr-2">TIME IN:</label>
                    <label for="year">Start Date</label>
                    <input class="form-control mb-2 mr-sm-2" type="text" id="in_start_date" required>
                    <label for="violation">End Date</label>
                    <input class="form-control mb-2 mr-sm-2" type="text" id="in_end_date" required>
                    <button type="submit" id="submit" class="btn btn-primary mb-2">Generate</button>
                </div>
            </div>
            {{-- <div class="col-md-8">
                <div class="form-inline">
                    <label class="mr-2">TIME OUT:</label>
                    <label for="year">Start Date</label>
                    <input class="form-control mb-2 mr-sm-2" type="text" id="out_start_date" required>
                    <label for="violation">End Date</label>
                    <input class="form-control mb-2 mr-sm-2" type="text" id="out_end_date" required>
                </div>
            </div> --}}
            <div class="col-md-12" style="overflow-x:auto;">
                <table class="table table-sm nowrap" id="table" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID Number</th>
                            <th>Time In</th>
                            <th>Time Out</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    $(document).ready(function () {


        $('#submit').click(function () {
            let in_start = $('#in_start_date').val();
            let in_end = $('#in_end_date').val();
            let out_start = $('#out_start_date').val();
            let out_end = $('#out_end_date').val();
            let url = decodeURIComponent("{{route('reports.logs.get')}}?in_start_date=:in_start&in_end_date=:in_end");
            console.log(url.replace(':in_start', in_start).replace(':in_end', in_end));
            table.ajax.url(url.replace(':in_start', in_start).replace(':in_end', in_end));
            table.ajax.reload();
        });

        let table = $('#table').DataTable({
            ajax: {
                url: "{{route('reports.logs.get')}}",
                dataSrc: ''
            },
            columns: [
                {data: 'id_number'},
                {data: 'time_in'},
                {data: 'time_out'},
            ],
            dom: 'fBrtlip',
            buttons: [
                {
                    text: '<i class="fas fa-file-csv"></i> CSV',
                    extend: 'csv',
                    className: 'btn btn-success mx-1 rounded-pill',
                    title: 'Report List'
                },
                {
                    text: '<i class="fas fa-file-pdf"></i> PDF',
                    extend: 'pdf',
                    className: 'btn btn-success mx-1 rounded-pill',
                    title: 'Report List'
                },
            ]
        });
    });
</script>
@endsection
