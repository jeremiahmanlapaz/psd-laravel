@extends('layouts.admin')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Violations</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered" id="table"></table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">Add Violation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('violations.create') }}" method="post" id="form">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title"
                            class="form-control @error('title') is-invalid @enderror">
                        @error('title')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="type">Type</label>
                        <select class="form-control" name="type" id="type">
                            <option value="Minor">Minor</option>
                            <option value="Major">Major</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="1st_offense">1st Offense Penalty/Sanction</label>
                        <input type="text" name="1st_offense" id="1st_offense"
                            class="form-control @error('1st_offense') is-invalid @enderror">
                        @error('1st_offense')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="2nd_offense">2nd Offense Penalty/Sanction</label>
                        <input type="text" name="2nd_offense" id="2nd_offense"
                            class="form-control @error('2nd_offense') is-invalid @enderror">
                        @error('2nd_offense')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="3rd_offense">3rd Offense Penalty/Sanction</label>
                        <input type="text" name="3rd_offense" id="3rd_offense"
                            class="form-control @error('3rd_offense') is-invalid @enderror">
                        @error('3rd_offense')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importModalLabel">Add Violation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('violations.import') }}" method="post" id="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="custom-file">
                        <input type="file" name="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        let table = $('#table').DataTable({
            ajax: {
                url: "{{route('violations.all')}}",
                dataSrc: ''
            },
            columns: [
                {data: 'title', title: 'Title'},
                {data: 'type', title: 'Type'},
                {data: 'first_offense', title: 'First Offense'},
                {data: 'second_offense', title: 'Second Offense'},
                {data: 'third_offense', title: 'Third Offense'},
                {data: null, title: 'Action', render: function (data) {
                    let url = '{{route("violations.edit", ["id" => ":id"])}}';
                    return '<a class="btn btn-warning btn-sm" href=' + url.replace(":id", data.id) + '><i class="fas fa-pencil-alt"></i> Edit</a>';
                }},
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fa fa-plus s"></i> Add Violation',
                    className: 'btn btn-success btn-sm rounded-pill mx-1',
                    action: function () {
                        $('#createModal').modal('show');
                    }
                },
                {
                    text: '<i class="fa fa-file-csv "></i> Import Violations',
                    className: 'btn btn-success btn-sm rounded-pill mx-1',
                    action: function () {
                        $('#importModal').modal('show');
                    }
                }
            ],
            ordering: [[1, 'desc'], [0, 'desc']],
            scrollX: true
        });

        $('#form').submit( function (e) {
            e.preventDefault();
            let data = $(this).serialize();
            $.ajax({
                url: "{{route('violations.create')}}",
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: data,
                success: function (data, status) {
                    if(data.message == "success") {
                        alertify.success(data.data['message']);
                    }else{
                        alertify.error(data.data['message']);
                    }
                },
                error: function (jqxhr, status) {
                    console.log(jqxhr);
                },
                complete: function () {
                    $('#createModal').modal('hide');
                }
            });
        });
    });
</script>
@endsection
