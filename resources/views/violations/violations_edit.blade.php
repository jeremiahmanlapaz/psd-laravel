@extends('layouts.admin')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Violations / Edit</h6>
    </div>
    <div class="card-body">
        <form method="post">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror"
                    value="{{$violation->title}}">
                @error('title')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="type">Type</label>
                <select class="form-control" name="type" id="type">
                    <option value="Minor" {{$violation->title == "Minor" ? 'selected':null}}>Minor</option>
                    <option value="Major" {{$violation->title == "Major" ? 'selected':null}}>Major</option>
                </select>
            </div>
            <div class="form-group">
                <label for="1st_offense">1st Offense Penalty/Sanction</label>
                <input type="text" name="1st_offense" id="1st_offense"
                    class="form-control @error('1st_offense') is-invalid @enderror"
                    value="{{$violation['1st_offense']}}">
                @error('1st_offense')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="2nd_offense">2nd Offense Penalty/Sanction</label>
                <input type="text" name="2nd_offense" id="2nd_offense"
                    class="form-control @error('2nd_offense') is-invalid @enderror"
                    value="{{$violation['2nd_offense']}}">
                @error('2nd_offense')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="3rd_offense">3rd Offense Penalty/Sanction</label>
                <input type="text" name="3rd_offense" id="3rd_offense"
                    class="form-control @error('3rd_offense') is-invalid @enderror"
                    value="{{$violation['3rd_offense']}}">
                @error('3rd_offense')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-warning"><i class="fas fa-save"></i> Save</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')

@endsection