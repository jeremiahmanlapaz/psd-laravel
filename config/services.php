<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'glabs' => [
        //  account: PSDUMAK@gmail.com
        //  number: 09950554925
        'short_code' => '1407',
        'app_id' => '5XKahaj97jF5bcbLX5T9yAFzMXadhbjE',
        'app_secret' => '1c128616862ec9979351455d67b815e47523a12ab335aacb6a8f3e3e81cc7657',

        //09276387225
        // 'access_token' => 'd8SkDucIX10Vj26pIqKwoZ2EDzQa-SEBYqAjoMkn5xg',
        // 'code' => '8CK89egFArL5Rhoq9AjUy8pkAsdgnExFRa4AMfBKMMESKga57Sb6kyqIrG6aRFb9Gn7fRoq5nSE9jL7tgpEM6ULKxnqhBRB58sMbo8dtqpRGbtkydMgSz6yoGhRdiA8oM6ATyMXhR9dbdS8LRXptaBoaktzjB86sbkxR8hjkE9MUXyj9RtGnq7MSp4GxEfk867kFMekkKIjLaXpSBEMrGSxd4LxfX5nBaF47p6bsnk9oyU7bL9gha59GzFA5p6zC',
        
        //09950554925
        'access_token' => 'yL3WktGT7QQcE59LkvmU4UTk8ZVWPI3Elnv8S0D7Tt4',
        'code' => 'XsxqgAqfXyLyqCAr7q7t4anqLtaoGBjFa55kyhRrMMLCaXeb7FKrgMAFX5L59hagA7kFk4ME5Fnk7peHLkGBXSo8LeBS8poqrhg6rojIRjBorf9akRxf7G97jF5bTb7LrdKc9yAFzMkyLfGpBMdfz4r86ILXobdhnbLrBSKRGbBSny7LzH6zMKqFabA7RFRoLAGh8Gg6bFXbeg8FBGMpqC7a5nyh74GMrFEjndjtgA7yEtLKL4XC8ggx6faGg9Ms'
    ],

];
